exports.handle_response = function handle_response(data) {
  var encrypted_data = data.encrypted_data,
    checksum = data.checksum;
  let cs = require('../crypt/checksum.js');
  if (cs.checksum(JSON.stringify(encrypted_data)) == checksum) {
    let dc = require('../crypt/decrypt_data.js'),
      decrypted_data = JSON.parse(dc.decrypt_data(encrypted_data));
    if (decrypted_data) {
      return decrypted_data;
    } else {
      return null;
    }
  } else {
    return null;
  }
}
