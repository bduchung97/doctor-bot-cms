exports.delete_object_from_object_array = function delete_object_from_object_array(object_array, USER_ID) {
  let removeIndex = object_array.map(function (item) { return item._id; }).indexOf(USER_ID);
  if (removeIndex != -1) {
    object_array.splice(removeIndex, 1);
    return object_array;
  }
  else return object_array;
}