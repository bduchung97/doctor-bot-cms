import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import axios from 'axios';

import { css } from '@emotion/core';

import { HashLoader } from 'react-spinners';

import API from '../../api/api_config';

import HomePage from '../home_page/HomePage';

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const styles = {
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/51472264_2172602326388009_702518454757883904_n.jpg?_nc_cat=103&_nc_oc=AQn_0TgjekAky1hAecICTEqkN3C62xggNG98vUurxSeHfd3dxWIApqiB1M6qrRsx_D8&_nc_ht=scontent.fhan2-4.fna&oh=7501b0572a5fd2cfeb626d385a523730&oe=5DDD20FD)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    marginLeft: '5%',
    marginRight: '5%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    marginTop: '10%',
    marginBottom: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 48,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: '5%',
  },
  submit: {
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 48,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    marginBottom: '2%',
  },
  loader: {
    alignSelf: 'center',
    marginTop: '5%',
    marginBottom: '5%'
  }
};

class LoginPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      dialogLoading: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputPassword = this.handleInputPassword.bind(this);
    this.handleInputUserName = this.handleInputUserName.bind(this);
  }

  handleInputUserName(event) {
    this.setState({
      username: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputPassword(event) {
    this.setState({
      password: event.target.value.replace(/^\s+/g, '')
    });
  }

  handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      this.handleSubmit();
    }
  }

  handleSubmit() {
    if (this.state.username === "") {
      alert("Mời bạn điền Username!");
    }
    else if (this.state.password === "") {
      alert("Mời bạn điền Password!");
    }
    else {
      this.setState({ dialogLoading: true });
      let data = {
        username: this.state.username,
        password: this.state.password
      }
      // axios.post(API.login_api, { data: data })
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.login_api, hreq._request(data))
        .then(response => {
          this.setState({ dialogLoading: false });
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          // var res_data = response.data;
          if (res_data.msg_code == '001') {
            var admin_data = res_data.data.admin_data,
              session_token = res_data.data.session_token;
            sessionStorage.setItem('admin_id', admin_data._id);
            sessionStorage.setItem('full_name', admin_data.FULL_NAME);
            sessionStorage.setItem('phone_number', admin_data.PHONE_NUMBER);
            sessionStorage.setItem('email', admin_data.EMAIL);
            sessionStorage.setItem('permission', admin_data.PERMISSION);
            sessionStorage.setItem('username', admin_data.USERNAME);
            sessionStorage.setItem('session_token', session_token);
            ReactDOM.render(<HomePage />, document.getElementById('root'));
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Tên tài khoản hoặc mật khẩu không đúng!')
          }
        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // if (error.response.status == 400) {
          //   alert('Đầu vào không đúng định dạng. Xin vui lòng thử lại!');
          // }
          // if (error.response.status == 500) {
          //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          // }
        });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Grid container component="main" className={classes.root} onKeyDown={this.handleKeyDown}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Hệ thống quản trị nội dung
            </Typography>
            <form className={classes.form} noValidate>
              <TextField
                //variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Tên đăng nhập"
                name="username"
                autoComplete="username"
                autoFocus
                value={this.state.username}
                onChange={this.handleInputUserName}
              />
              <TextField
                //variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Mật khẩu"
                type="password"
                id="password"
                autoComplete="current-password"
                value={this.state.password}
                onChange={this.handleInputPassword}
              />
              {/* <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Ghi nhớ đăng nhập"
              /> */}
              <Button
                // type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={this.handleSubmit}
              >
                Đăng nhập
              </Button>
              {/* <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Quên mật khẩu?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2">
                    {"Đăng ký tài khoản mới với admin."}
                  </Link>
                </Grid>
              </Grid> */}
              <Box mt={5}>
                <Typography variant="body2" color="textSecondary" align="center">
                  {'Phát triển bởi TW4N.'}
                </Typography>
              </Box>
            </form>
          </div>
        </Grid>
      </Grid>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LoginPage);