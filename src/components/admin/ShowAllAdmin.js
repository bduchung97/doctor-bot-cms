import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const tableIcons = {
  Add: AddBox,
  Check: Check,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  },
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#52d869',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
});

class ShowAllAdmin extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      permission: sessionStorage.getItem('permission').split(","),
      admin_id: '',
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      dialogCreateAdmin: false,
      dialogUpdateAdmin: false,
      admin_array: [],
      old_admin: null,
      columns: [
        { title: 'Họ tên', field: 'FULL_NAME' },
        { title: 'Số điện thoại', field: 'PHONE_NUMBER' },
        { title: 'Email', field: 'EMAIL' },
        { title: 'Tên đăng nhập', field: 'USERNAME', },
      ],
      create_username: '',
      create_password: '',
      create_fullname: '',
      create_email: '',
      create_phonenumber: '',
      create_permission: [],
      check_ADMN: false,
      check_USER: false,
      check_GROU: false,
      check_DATA: false,
      check_AITR: false,
      check_HAND: false,
      check_NOTI: false,
      check_SCHE: false,
      check_STAT: false,
    };
    this.handleInputCreateUsername = this.handleInputCreateUsername.bind(this);
    this.handleInputCreatePassword = this.handleInputCreatePassword.bind(this);
    this.handleInputCreateFullname = this.handleInputCreateFullname.bind(this);
    this.handleInputCreateEmail = this.handleInputCreateEmail.bind(this);
    this.handleInputCreatePhoneNumber = this.handleInputCreatePhoneNumber.bind(this);
  }

  handleInputCreateUsername(event) {
    this.setState({
      create_username: event.target.value.replace(/^\s+/g, '').split(' ').join('')
    });
  }
  handleInputCreatePassword(event) {
    this.setState({
      create_password: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputCreateFullname(event) {
    this.setState({
      create_fullname: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputCreateEmail(event) {
    this.setState({
      create_email: event.target.value.replace(/^\s+/g, '').split(' ').join('')
    });
  }
  handleInputCreatePhoneNumber(event) {
    this.setState({
      create_phonenumber: event.target.value.replace(/^\s+/g, '').split(' ').join('')
    });
  }

  handleChangeADMN(event) {
    this.setState({ check_ADMN: event.target.checked });
  }
  handleChangeAITR(event) {
    this.setState({ check_AITR: event.target.checked });
  }
  handleChangeDATA(event) {
    this.setState({ check_DATA: event.target.checked });
  }
  handleChangeGROU(event) {
    this.setState({ check_GROU: event.target.checked });
  }
  handleChangeHAND(event) {
    this.setState({ check_HAND: event.target.checked });
  }
  handleChangeNOTI(event) {
    this.setState({ check_NOTI: event.target.checked });
  }
  handleChangeSCHE(event) {
    this.setState({ check_SCHE: event.target.checked });
  }
  handleChangeSTAT(event) {
    this.setState({ check_STAT: event.target.checked });
  }
  handleChangeUSER(event) {
    this.setState({ check_USER: event.target.checked });
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
    this.getAllAdmin();
  }

  getAllAdmin() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_all_admin, hreq._request('', this.state.session_token))
      // axios.post(API.show_all_admin, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var aa = res_data.admins_data;
          for (let i = 0; i < aa.length; i++) {
            if (aa[i].IS_ACTIVE == 1) {
              aa[i].IS_ACTIVE = 'Đang hoạt động'
            } else {
              aa[i].IS_ACTIVE = 'Đã bị xoá'
            }
          }
          this.setState({
            admin_array: aa,
            dialogCreateAdmin: false,
            dialogUpdateAdmin: false,
            old_admin: null,
            create_username: '',
            create_password: '',
            create_fullname: '',
            create_email: '',
            create_phonenumber: '',
            create_permission: [],
            check_ADMN: false,
            check_USER: false,
            check_GROU: false,
            check_DATA: false,
            check_AITR: false,
            check_HAND: false,
            check_NOTI: false,
            check_SCHE: false,
            check_STAT: false,
          });
          this.setState({ dialogLoading: false });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  onClickDeleteAdmin = (event, rowData) => {
    if (rowData.USERNAME == 'root') {
      alert('Không thể xoá admin root!');
    } else if (rowData._id == this.state.admin_id) {
      alert('Không thể tự xoá chính mình!');
    }
    else {
      this.setState({ dialogLoading: true });

      let data = {
        data: rowData._id,
        admin_id: this.state.admin_id
      };
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.delete_admin, hreq._request(data, this.state.session_token))
        // axios.post(API.delete_admin, { data: data, session_token: this.state.session_token })
        .then(response => {
          this.setState({ dialogLoading: false });
          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.getAllAdmin();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          }
        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // if (error.response.status == 400) {
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
          // if (error.response.status == 500) {
          //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
        });
    }
  }

  updateAdmin = () => {
    if (this.state.create_fullname == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.create_email == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.create_phonenumber == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else {
      this.setState({ dialogLoading: true });
      if (this.state.check_ADMN == true) {
        this.state.create_permission.push('ADMN');
      }
      if (this.state.check_AITR == true) {
        this.state.create_permission.push('AITR');
      }
      if (this.state.check_DATA == true) {
        this.state.create_permission.push('DATA');
      }
      if (this.state.check_GROU == true) {
        this.state.create_permission.push('GROU');
      }
      if (this.state.check_HAND == true) {
        this.state.create_permission.push('HAND');
      }
      if (this.state.check_NOTI == true) {
        this.state.create_permission.push('NOTI');
      }
      if (this.state.check_SCHE == true) {
        this.state.create_permission.push('SCHE');
      }
      if (this.state.check_STAT == true) {
        this.state.create_permission.push('STAT');
      }
      if (this.state.check_USER == true) {
        this.state.create_permission.push('USER');
      }
      let pass = this.state.create_password;
      if (pass == '') {
        pass = null
      }

      let data = {
        admin_ID: this.state.old_admin._id,
        updated_data: {
          PASSWORD: pass,
          FULL_NAME: this.state.create_fullname,
          EMAIL: this.state.create_email,
          PHONE_NUMBER: this.state.create_phonenumber,
          PERMISSION: this.state.create_permission,
          UPDATED_BY: this.state.admin_id,
          UPDATED_AT: new Date()
        }
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.put(API.update_admin, hreq._request(data, this.state.session_token))
        // axios.put(API.update_admin, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogUpdateAdmin: false,
              create_email: '',
              create_phonenumber: '',
              check_ADMN: false,
              check_USER: false,
              check_GROU: false,
              check_DATA: false,
              check_AITR: false,
              check_HAND: false,
              check_NOTI: false,
              check_SCHE: false,
              check_STAT: false,
              old_admin: null,
            });
            this.getAllAdmin();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({
              dialogUpdateAdmin: false,
              create_email: '',
              create_phonenumber: '',
              check_ADMN: false,
              check_USER: false,
              check_GROU: false,
              check_DATA: false,
              check_AITR: false,
              check_HAND: false,
              check_NOTI: false,
              check_SCHE: false,
              check_STAT: false,
              old_admin: null,
              dialogLoading: false,
            })
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });

    }
  }

  onClickCreateAdmin = () => {
    if (this.state.create_fullname == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.create_username == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.create_password == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.create_email == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.create_phonenumber == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else {
      this.setState({ dialogLoading: true });
      if (this.state.check_ADMN == true) {
        this.state.create_permission.push('ADMN');
      }
      if (this.state.check_AITR == true) {
        this.state.create_permission.push('AITR');
      }
      if (this.state.check_DATA == true) {
        this.state.create_permission.push('DATA');
      }
      if (this.state.check_GROU == true) {
        this.state.create_permission.push('GROU');
      }
      if (this.state.check_HAND == true) {
        this.state.create_permission.push('HAND');
      }
      if (this.state.check_NOTI == true) {
        this.state.create_permission.push('NOTI');
      }
      if (this.state.check_SCHE == true) {
        this.state.create_permission.push('SCHE');
      }
      if (this.state.check_STAT == true) {
        this.state.create_permission.push('STAT');
      }
      if (this.state.check_USER == true) {
        this.state.create_permission.push('USER');
      }
      let data = {
        fullname: this.state.create_fullname,
        username: this.state.create_username,
        password: this.state.create_password,
        email: this.state.create_email,
        phone_number: this.state.create_phonenumber,
        permission: this.state.create_permission,
        admin_id: this.state.admin_id,
      }
      console.log(data);
      // axios.post(API.create_admin, { data: data, session_token: this.state.session_token })
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.create_admin, hreq._request(data, this.state.session_token))
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              create_username: '',
              create_fullname: '',
              create_password: '',
              create_email: '',
              create_phonenumber: '',
              create_permission: [],
              check_ADMN: false,
              check_USER: false,
              check_GROU: false,
              check_DATA: false,
              check_AITR: false,
              check_HAND: false,
              check_NOTI: false,
              check_SCHE: false,
              check_STAT: false,
              dialogCreateAdmin: false,
            })
            this.getAllAdmin();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Trùng username!');
            this.setState({ dialogLoading: false });
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // if (error.response.status == 400) {
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
          // if (error.response.status == 500) {
          //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
        });
    }
  }

  onClickUpdateAdmin = (event, rowData) => {
    if (rowData.IS_ACTIVE == 'Đã bị xoá') {
      alert('Admin đã xoá, không thể chỉnh sửa!')
    } else {
      if (rowData.USERNAME == 'root') {
        alert('Không thể chỉnh sửa admin root')
      } else {
        this.setState({
          dialogUpdateAdmin: true,
          create_fullname: rowData.FULL_NAME,
          create_email: rowData.EMAIL,
          create_phonenumber: rowData.PHONE_NUMBER,
          old_admin: rowData
        });
        if (rowData.PERMISSION.indexOf('ADMN') !== -1) {
          this.setState({ check_ADMN: true });
        }
        if (rowData.PERMISSION.indexOf('AITR') !== -1) {
          this.setState({ check_AITR: true });
        }
        if (rowData.PERMISSION.indexOf('DATA') !== -1) {
          this.setState({ check_DATA: true });
        }
        if (rowData.PERMISSION.indexOf('GROU') !== -1) {
          this.setState({ check_GROU: true });
        }
        if (rowData.PERMISSION.indexOf('HAND') !== -1) {
          this.setState({ check_HAND: true });
        }
        if (rowData.PERMISSION.indexOf('NOTI') !== -1) {
          this.setState({ check_NOTI: true });
        }
        if (rowData.PERMISSION.indexOf('SCHE') !== -1) {
          this.setState({ check_SCHE: true });
        }
        if (rowData.PERMISSION.indexOf('STAT') !== -1) {
          this.setState({ check_STAT: true });
        }
        if (rowData.PERMISSION.indexOf('USER') !== -1) {
          this.setState({ check_USER: true });
        }
      }
    }
  }

  onCloseDialogUpdateAdmin = () => {
    this.setState({
      dialogUpdateAdmin: false,
      create_email: '',
      create_phonenumber: '',
      check_ADMN: false,
      check_USER: false,
      check_GROU: false,
      check_DATA: false,
      check_AITR: false,
      check_HAND: false,
      check_NOTI: false,
      check_SCHE: false,
      check_STAT: false,
      old_admin: null
    });
  }

  onCloseDialogCreateAdmin = () => {
    this.setState({
      dialogCreateAdmin: false,
      create_username: '',
      create_password: '',
      create_fullname: '',
      create_email: '',
      create_phonenumber: '',
      create_permission: [],
      check_ADMN: false,
      check_USER: false,
      check_GROU: false,
      check_DATA: false,
      check_AITR: false,
      check_HAND: false,
      check_NOTI: false,
      check_SCHE: false,
      check_STAT: false,
    });
  }

  createAdmin = () => {
    this.setState({ dialogCreateAdmin: true });
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.dialogCreateAdmin}
          //onClose={this.onCloseDialogCreateAdmin}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm admin</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="username"
              label="Tên đăng nhập"
              // type="password"
              id="username"
              autoComplete="current-username"
              value={this.state.create_username}
              onChange={this.handleInputCreateUsername}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Mật khẩu"
              type="password"
              id="password"
              autoComplete="current-password"
              value={this.state.create_password}
              onChange={this.handleInputCreatePassword}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="fullname"
              label="Họ tên"
              // type="password"
              id="fullname"
              autoComplete="current-fullname"
              value={this.state.create_fullname}
              onChange={this.handleInputCreateFullname}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="email"
              label="Email"
              // type="password"
              id="email"
              autoComplete="current-email"
              value={this.state.create_email}
              onChange={this.handleInputCreateEmail}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="phonenumber"
              label="Số điện thoại"
              // type="password"
              id="phonenumber"
              autoComplete="current-phonenumber"
              value={this.state.create_phonenumber}
              onChange={this.handleInputCreatePhoneNumber}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_ADMN}
                      onChange={event => this.handleChangeADMN(event)}
                      color="primary"
                    />
                  }
                  label="Quyền quản trị admin"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_AITR}
                      onChange={event => this.handleChangeAITR(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền huấn luyện AI"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_DATA}
                      onChange={event => this.handleChangeDATA(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị dữ liệu Chatbot"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_GROU}
                      onChange={event => this.handleChangeGROU(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị nhóm"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_HAND}
                      onChange={event => this.handleChangeHAND(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị sổ tay"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_NOTI}
                      onChange={event => this.handleChangeNOTI(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị thông báo"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_SCHE}
                      onChange={event => this.handleChangeSCHE(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền lập lịch huấn luyện AI"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_STAT}
                      onChange={event => this.handleChangeSTAT(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị thống kê"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_USER}
                      onChange={event => this.handleChangeUSER(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị người dùng"
                />
              </Grid>
              <Grid item>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogCreateAdmin}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickCreateAdmin}>
              Tạo
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogUpdateAdmin}
          //onClose={this.onCloseDialogUpdateAdmin}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa thông tin admin</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="fullname"
              label="Họ tên"
              // type="password"
              id="fullname"
              autoComplete="current-fullname"
              value={this.state.create_fullname}
              onChange={this.handleInputCreateFullname}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="email"
              label="Email"
              // type="password"
              id="email"
              autoComplete="current-email"
              value={this.state.create_email}
              onChange={this.handleInputCreateEmail}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="phonenumber"
              label="Số điện thoại"
              // type="password"
              id="phonenumber"
              autoComplete="current-phonenumber"
              value={this.state.create_phonenumber}
              onChange={this.handleInputCreatePhoneNumber}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              // required
              fullWidth
              name="password"
              label="Cấp lại mật khẩu mới"
              type="password"
              id="password"
              autoComplete="current-password"
              value={this.state.create_password}
              onChange={this.handleInputCreatePassword}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_ADMN}
                      onChange={event => this.handleChangeADMN(event)}
                      color="primary"
                    />
                  }
                  label="Quyền quản trị admin"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_AITR}
                      onChange={event => this.handleChangeAITR(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền huấn luyện AI"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_DATA}
                      onChange={event => this.handleChangeDATA(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị dữ liệu Chatbot"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_GROU}
                      onChange={event => this.handleChangeGROU(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị nhóm"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_HAND}
                      onChange={event => this.handleChangeHAND(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị sổ tay"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_NOTI}
                      onChange={event => this.handleChangeNOTI(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị thông báo"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_SCHE}
                      onChange={event => this.handleChangeSCHE(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền lập lịch huấn luyện AI"
                />
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_STAT}
                      onChange={event => this.handleChangeSTAT(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị thống kê"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2} xs={12}>
              <Grid item style={{ width: '40%', marginLeft: '2%' }}>
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        root: classes.root,
                        switchBase: classes.switchBase,
                        thumb: classes.thumb,
                        track: classes.track,
                        checked: classes.checked,
                      }}
                      focusVisibleClassName={classes.focusVisible}
                      disableRipple
                      checked={this.state.check_USER}
                      onChange={event => this.handleChangeUSER(event)}
                      // value="checkedB"
                      color="primary"
                    />
                  }
                  label="Quyền quản trị người dùng"
                />
              </Grid>
              <Grid item>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUpdateAdmin}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateAdmin}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Danh sách Admin"
            columns={this.state.columns}
            data={this.state.admin_array}
            actions={[
              {
                icon: AddBox,
                tooltip: 'Thêm',
                isFreeAction: true,
                onClick: this.createAdmin
              },
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdateAdmin(event, rowData),
              }),
              rowData => ({
                icon: DeleteOutline,
                tooltip: 'Xoá',
                onClick: (event, rowData) => this.onClickDeleteAdmin(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1, paging: false
            }}
          />
        </CardContent>
        <CardActions >

        </CardActions>
      </Card>
    );
  }

  render() {
    if (this.state.permission.indexOf('ADMN') == -1) {
      return <h2>&emsp;&emsp;&emsp;Bạn không có quyền truy cập chức năng này!</h2>;
    } else {
      console.log(this.state.admin_array);
      const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    Quản lý Admin
                </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Tabs value={this.state.tab_value} textColor="inherit">
              <Tab textColor="inherit" label="Danh sách Admin" />
            </Tabs>
          </AppBar>
          {this.state.tab_value === 0 && this.renderContent()}
        </React.Fragment>
      );
    }
  }
}

ShowAllAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(ShowAllAdmin);