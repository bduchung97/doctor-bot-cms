import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ToggleOn from '@material-ui/icons/ToggleOn';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import Group from '@material-ui/icons/Group';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MuiTree from 'material-ui-tree';
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import CardMedia from '@material-ui/core/CardMedia';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SelectSearch from 'react-select';

import Nested_img from '../../assets/nested.png';
import Function_img from '../../assets/generic.png';
import Web_img from '../../assets/web.png';
import PersistentMenu_img from '../../assets/persistent-menu.png'

const tableIcons = {
  Add: AddBox,
  ToggleOn: ToggleOn,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn,
  Group: Group
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%',
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  media: {
    height: 200,
    width: 700
  },
  media_: {
    height: 260,
    marginBottom: '2%'
  },
  media_title: {
    width: 400,
    height: 400,
  }
});

class PersistentMenu extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      dialogShowUpdate: false,
      leaf_data: [],
      create_url: '',
      create_title: '',
      create_nested_title: '',
      create_function_id: '',
      index: null,
      template_id: '',
      showAction: false,
      showSelectionType: true,
      showWebUrlType: false,
      showFunctionType: false,
      showNestedType: false,
      showNestedType_WebUrl: false,
      showNestedType_FunctionID: false,
      node_array: [],
      columns: [
        { title: 'Nội dung', field: 'title', },
        { title: 'Loại', field: 'type' },
      ],
      dialogCreateRow: false,
      create_type: false,
      index: 0,
      dialogUpdateRow: false
    };
    this.handleInputTitle = this.handleInputTitle.bind(this);
    this.handleInputURL = this.handleInputURL.bind(this);
    this.handleInputFunctionID = this.handleInputFunctionID.bind(this);
    this.handleInputNestedTitle = this.handleInputNestedTitle.bind(this);
    this.handleInputType = this.handleInputType.bind(this);
  }

  handleInputNestedTitle(event) {
    this.setState({
      create_nested_title: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputTitle(event) {
    var value = event.target.value.replace(/^\s+/g, '');
    if (value.length > 30) {
      alert('Đã đạt giới hạn!')
    } else {
      this.setState({
        create_title: value
      });
    }

  }
  handleInputURL(event) {
    this.setState({
      create_url: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputType(event) {
    this.setState({
      create_type: event.target.value
    });
  }
  handleInputFunctionID(event) {
    this.setState({
      create_function_id: event
    });
  }

  componentDidMount() {
    this.getLeaf();
  }

  getLeaf() {
    let data = {
      function_id: 'PERSISTENT_MENU',
    }
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_leaf, hreq._request(data, this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var leaf_data = res_data.leaf_data[0].VALUE.persistent_menu[0].call_to_actions;

          leaf_data.map((e, i) => {
            if (e.type == 'postback') {
              e.type = 'Mở chức năng'
            } else {
              e.type = 'Mở website'
            }
          })

          this.setState({
            dialogLoading: false,
            leaf_data: leaf_data,
            template_id: res_data.leaf_data[0]._id,
            dialogShowUpdate: false,
            create_url: '',
            create_title: '',
            create_nested_title: '',
            create_function_id: '',
            index: null,
            showAction: false,
            showSelectionType: true,
            showWebUrlType: false,
            showFunctionType: false,
            showNestedType: false,
            showNestedType_WebUrl: false,
            showNestedType_FunctionID: false,
            node_array: [],
            columns: [
              { title: 'Nội dung', field: 'title', },
              { title: 'Loại', field: 'type' },
            ],
            dialogCreateRow: false,
            create_type: false,
            index: 0,
            dialogUpdateRow: false
          });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }

        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  getNode() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_node_all_, hreq._request('', this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var a = [];
          res_data.node_data.map((e, i) => {
            a.push({
              value: e.FUNCTION_ID,
              label: e.FUNCTION_ID + '- Mô tả: ' + e.DESCRIPTION
            })
          })
          this.setState({
            node_array: a,
            dialogLoading: false
          });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  handleSubmit() {
    this.setState({ dialogLoading: true });
    var value = this.state.leaf_data;
    if (this.state.create_title !== "") {
      if (this.state.create_type == true) {
        value.push({
          title: this.state.create_title,
          type: 'web_url',
          url: this.state.create_url,
          webview_height_ratio: "full"
        })
      } else {
        value.push({
          title: this.state.create_title,
          type: 'postback',
          payload: this.state.create_function_id.value
        })
      }
    }
    let data = {
      template_id: this.state.template_id,
      updated_data: {
        VALUE: {
          persistent_menu: [{
            locale: "default",
            composer_input_disabled: false,
            call_to_actions: value
          }]
        },
        UPDATED_BY: this.state.admin_id,
        UPDATED_AT: new Date()
      }

    }
    value.map((e, i) => {
      if (e.type == 'Mở chức năng') {
        e.type = 'postback';
      }
      if (e.type == 'Mở website') {
        e.type = 'web_url';
      }
      delete e.tableData
    })

    console.log(data);
    let hreq = require('../../utils/handle_request/_request');
    axios.put(API.update_persistent_menu, hreq._request(data, this.state.session_token))
      // axios.put(API.update_handbook, { data: data, session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.getLeaf();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({
            dialogLoading: false,
          })
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  handleUpdateRow = () => {
    var i = this.state.index;
    this.setState({ dialogLoading: true });
    var value = this.state.leaf_data;
    if (this.state.create_type == true) {
      value[i].title = this.state.create_title;
      value[i].type = 'web_url';
      value[i].url = this.state.create_url;
      value[i].webview_height_ratio = "full"
    } else {
      value[i].title = this.state.create_title;
      value[i].type = 'postback';
      value[i].payload = this.state.create_function_id.value;
    }

    let data = {
      template_id: this.state.template_id,
      updated_data: {
        VALUE: {
          persistent_menu: [{
            locale: "default",
            composer_input_disabled: false,
            call_to_actions: value
          }]
        },
        UPDATED_BY: this.state.admin_id,
        UPDATED_AT: new Date()
      }

    }
    value.map((e, i) => {
      if (e.type == 'Mở chức năng') {
        e.type = 'postback';
      }
      if (e.type == 'Mở website') {
        e.type = 'web_url';
      }
      delete e.tableData
    })

    console.log(data);
    let hreq = require('../../utils/handle_request/_request');
    axios.put(API.update_persistent_menu, hreq._request(data, this.state.session_token))
      // axios.put(API.update_handbook, { data: data, session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.getLeaf();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({
            dialogLoading: false,
          })
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  openDialogCreateRow = () => {
    if (this.state.leaf_data.length == 3) {
      alert('Đã đạt đến giới hạn!');
    } else {
      this.getNode();
      this.setState({
        dialogCreateRow: true
      })
    }
  }

  onClickUpdateRow = (event, rowData) => {
    console.log(rowData)
    this.getNode();
    if (rowData.type == 'Mở website') {
      this.setState({
        create_type: true,
        create_url: rowData.url,
      });
    } else {
      this.setState({
        create_type: false,
        create_function_id: {
          value: rowData.payload,
          label: rowData.payload
        },
      });
    }
    this.setState({
      create_title: rowData.title,
      dialogUpdateRow: true,
      index: rowData.tableData.id
    })
  }

  onCloseUpdateRow = () => {
    this.setState({
      dialogUpdateRow: false,
      create_type: false,
      create_function_id: '',
      create_title: '',
      create_url: ''
    })
  }

  onCloseDialogCreateRow = () => {
    this.setState({
      dialogCreateRow: false,
      create_function_id: '',
      create_title: '',
      create_url: ''
    })
  }

  onClickDeleteRow = (event, rowData) => {
    var leaf_data = this.state.leaf_data;
    leaf_data.splice(rowData.tableData.id, 1)
    this.setState({ leaf_data: leaf_data })
    this.handleSubmit()
  }

  render() {
    console.log(this.state.leaf_data);
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.dialogCreateRow}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm dòng</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="title"
              label="Nội dung"
              // type="password"
              id="title"
              autoComplete="current-title"
              value={this.state.create_title}
              onChange={this.handleInputTitle}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <InputLabel htmlFor="outlined-age-simple" fullWidth>
                Chọn loại
              </InputLabel>
              <Select
                value={this.state.create_type}
                onChange={this.handleInputType}
                input={<OutlinedInput fullWidth id="outlined-age-simple" />}
              >
                <MenuItem value={true}>Mở web</MenuItem>
                <MenuItem value={false}>Mở chức năng</MenuItem>
              </Select>
            </FormControl>
            <br />
            <br />
            {
              this.state.create_type ?
                <TextField
                  //variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="url"
                  label="Đường dẫn của nút"
                  // type="password"
                  id="url"
                  autoComplete="current-url"
                  value={this.state.create_url}
                  onChange={this.handleInputURL}
                  style={{ marginLeft: '2%', width: '90%' }}
                /> :
                <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                  <SelectSearch
                    placeholder="Mã chức năng"
                    style={{ marginLeft: '2%', width: '60%', }}
                    value={this.state.create_function_id}
                    onChange={this.handleInputFunctionID}
                    options={this.state.node_array}
                  />
                </FormControl>
            }
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogCreateRow}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={() => this.handleSubmit()}>
              Tạo
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogUpdateRow}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa dòng</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="title"
              label="Nội dung"
              // type="password"
              id="title"
              autoComplete="current-title"
              value={this.state.create_title}
              onChange={this.handleInputTitle}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <InputLabel htmlFor="outlined-age-simple" fullWidth>
                Chọn loại
              </InputLabel>
              <Select
                value={this.state.create_type}
                onChange={this.handleInputType}
                input={<OutlinedInput fullWidth id="outlined-age-simple" />}
              >
                <MenuItem value={true}>Mở web</MenuItem>
                <MenuItem value={false}>Mở chức năng</MenuItem>
              </Select>
            </FormControl>
            <br />
            <br />
            {
              this.state.create_type ?
                <TextField
                  //variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="url"
                  label="Đường dẫn của nút"
                  // type="password"
                  id="url"
                  autoComplete="current-url"
                  value={this.state.create_url}
                  onChange={this.handleInputURL}
                  style={{ marginLeft: '2%', width: '90%' }}
                /> :
                <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                  <SelectSearch
                    placeholder="Mã chức năng"
                    style={{ marginLeft: '2%', width: '60%', }}
                    value={this.state.create_function_id}
                    onChange={this.handleInputFunctionID}
                    options={this.state.node_array}
                  />
                </FormControl>
            }
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseUpdateRow}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={() => this.handleUpdateRow()}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Menu nhanh"
            columns={this.state.columns}
            data={this.state.leaf_data}
            style={{}}
            actions={[
              {
                icon: AddBox,
                tooltip: 'Thêm',
                isFreeAction: true,
                onClick: this.openDialogCreateRow
              },
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdateRow(event, rowData),
              }),
              rowData => ({
                icon: DeleteOutline,
                tooltip: 'Xoá',
                onClick: (event, rowData) => this.onClickDeleteRow(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1,
              paging: false
            }}
          />
        </CardContent>
      </Card>
    )
  }
}
PersistentMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(PersistentMenu);