import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SortableTree from 'react-sortable-tree';
import 'react-sortable-tree/style.css';
import SelectSearch from 'react-select';

const tableIcons = {
  Add: AddBox,
  Check: Check,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  },
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#52d869',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
});

const handle_vi = require('../../utils/handle_vie/convert_vi')

class OthersNode extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      permission: sessionStorage.getItem('permission').split(","),
      session_token: sessionStorage.getItem('session_token'),
      admin_id: '',
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      dialogLoading: true,
      dialogCreateNode: false,
      dialogUpdateNode: false,
      dialogUploadHandbook: false,
      showTreeView: false,
      tree: [],
      node_array: [],
      node_array_raw: [],
      file: [],
      old_node: null,
      columns: [
        { title: 'Mã chức năng', field: 'FUNCTION_ID' },
        { title: 'Mã chức năng cha', field: 'PARENT_FUNCTION__ID' },
        { title: 'Mô tả', field: 'DESCRIPTION' },
      ],
      create_function_id: '',
      create_parent_function_id: '',
      create_description: '',
      dialogUpload: false,
    };
    this.handleInputFunctionId = this.handleInputFunctionId.bind(this);
    this.handleInputParentFunctionId = this.handleInputParentFunctionId.bind(this);
    this.handleInputDescription = this.handleInputDescription.bind(this);
  }

  handleChangeTreeView(event) {
    this.setState({
      showTreeView: event.target.checked
    });
  }

  handleInputFunctionId(event) {
    var a = handle_vi.change_alias(event.target.value.replace(/^\s+/g, ''));
    this.setState({
      create_function_id: a.toUpperCase()
    });
  }
  handleInputParentFunctionId(event) {
    this.setState({
      create_parent_function_id: event
    });
  }
  handleInputDescription(event) {
    this.setState({
      create_description: event.target.value.replace(/^\s+/g, '')
    });
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
    this.getAllNode();
  }

  getAllNode() {
    // axios.post(API.show_all_handbook, { data: '', session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_node_all_, hreq._request('', this.state.session_token))
      .then(response => {
        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var a = [];
          res_data.node_data.map((e, i) => {
            a.push({
              value: e.FUNCTION_ID,
              label: e.FUNCTION_ID + '- Mô tả: ' + e.DESCRIPTION
            })
          })
          this.setState({
            node_array_raw: res_data.node_data,
            node_array: a,
            showTreeView: false,
            tab_value: 0,
            fullname: '',
            username: '',
            phonenumber: '',
            email: '',
            dialogLoading: true,
            dialogCreateNode: false,
            dialogUpdateNode: false,
            dialogUploadHandbook: false,
            showTreeView: false,
            file: [],
            old_node: null,
            columns: [
              { title: 'Mã chức năng', field: 'FUNCTION_ID' },
              { title: 'Mã chức năng cha', field: 'PARENT_FUNCTION__ID' },
              { title: 'Mô tả', field: 'DESCRIPTION' },
            ],
            create_function_id: '',
            create_parent_function_id: '',
            create_description: '',
            dialogUpload: false,
          });
          this.setState({ dialogLoading: false });
          var tree = [];
          for (let i = 0; i < res_data.node_data.length; i++) {
            tree.push({
              title: res_data.node_data[i].FUNCTION_ID,
              subtitle: res_data.node_data[i].DESCRIPTION,
              parent_title: res_data.node_data[i].PARENT_FUNCTION__ID,
              expanded: true,
            })
          }
          var tree_ = function (data, root) {
            var r = [], o = {};
            data.forEach(function (a) {
              if (o[a.title] && o[a.title].children) {
                a.children = o[a.title] && o[a.title].children;
              }
              o[a.title] = a;
              if (a.parent_title === root) {
                r.push(a);
              } else {
                o[a.parent_title] = o[a.parent_title] || {};
                o[a.parent_title].children = o[a.parent_title].children || [];
                o[a.parent_title].children.push(a);
              }
            });
            return r;
          }(tree, 'GET_STARTED');

          for (let i = 0; i < tree_.length; i++) {
            delete tree_[i].parent_title;
            Object.assign(tree_, { expanded: true })
          }
          this.setState({ tree: tree_ })
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });
  }

  onClickDeleteNode = (event, rowData) => {
    this.setState({ dialogLoading: true });
    let data = {
      node_id: rowData._id,
    };
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.delete_leaf, hreq._request(data, this.state.session_token))
      // axios.post(API.delete_handbook, { data: data, session_token: this.state.session_token })
      .then(response => {
        this.setState({ dialogLoading: false });

        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        // var res_data = response.data;
        if (res_data.msg_code == '001') {
          this.getAllNode();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });
  }

  updateNode = () => {
    if (this.state.create_description == '') {
      alert('Bạn phải điền đầy đủ thông tin!');
    }
    // else if (this.state.create_parent_function_id.value) {
    //   alert('Bạn phải điền đầy đủ thông tin!');
    // }
    else {
      this.setState({ dialogLoading: true });
      let data = {
        node_id: this.state.old_node._id,
        updated_data: {
          PARENT_FUNCTION__ID: this.state.create_parent_function_id.value,
          DESCRIPTION: this.state.create_description,
          // FUNCTION_ID: this.state.create_function_id,
          UPDATED_BY: this.state.admin_id,
          UPDATED_AT: new Date()
        }
      }
      if (this.state.old_node.FUNCTION_ID !== this.state.create_function_id) {
        Object.assign(data.updated_data, { FUNCTION_ID: this.state.create_function_id })
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.put(API.update_node, hreq._request(data, this.state.session_token))
        // axios.put(API.update_handbook, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogUpdateNode: false,
              create_parent_function_id: '',
              create_description: '',
              create_function_id: '',
              old_node: null,
            });
            this.getAllNode();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({
              dialogUpdateNode: false,
              create_parent_function_id: '',
              create_description: '',
              create_function_id: '',
              old_node: null,
              dialogLoading: false,
            })
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // if (error.response.status == 400) {
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
          // if (error.response.status == 500) {
          //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
        });
    }

  }

  onClickCreateNode = () => {
    if (this.state.create_function_id == '') {
      alert(this.state.create_function_id)
      alert('Bạn phải điền đầy đủ thông tin!');
    }
    else if (this.state.create_description == '') {
      alert('Bạn phải điền đầy đủ thông tin!');
    }
    // else if (this.state.create_parent_function_id.value) {
    //   alert('Bạn phải điền đầy đủ thông tin!');
    // }
    else {
      this.setState({ dialogLoading: true });
      let data = {
        function_id: this.state.create_function_id,
        parent_function_id: this.state.create_parent_function_id.value,
        value: null,
        created_by: this.state.admin_id,
        description: this.state.create_description,
        type: null,
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.create_leaf, hreq._request(data, this.state.session_token))
        // axios.post(API.create_handbook, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              create_function_id: '',
              create_description: '',
              create_parent_function_id: '',
              dialogCreateNode: false,
            })
            this.getAllNode();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({
              create_function_id: '',
              create_description: '',
              create_parent_function_id: '',
              dialogCreateNode: false,
            })
            this.setState({ dialogLoading: false });
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // if (error.response.status == 400) {
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
          // if (error.response.status == 500) {
          //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          //   sessionStorage.removeItem('admin_id');
          //   sessionStorage.removeItem('full_name');
          //   sessionStorage.removeItem('phone_number');
          //   sessionStorage.removeItem('email');
          //   sessionStorage.removeItem('permission');
          //   sessionStorage.removeItem('username');
          //   sessionStorage.removeItem('session_token');
          //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
          // }
        });
    }
  }

  onClickUploadNode = () => {
    if (!this.state.file[0]) {
      alert("Bạn chưa chọn file!")
    }
    else {
      if (this.state.file[0].name === 'node.xlsx') {
        this.setState({ dialogLoading: true });
        let formData = new FormData();
        formData.append('session_token', this.state.session_token);
        formData.append('file', this.state.file[0]);
        formData.append('file_name', this.state.file[0].name);
        formData.append('admin_id', this.state.admin_id);
        axios.post(API.upload_node, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
          .then(response => {

            // var res_data = response.data;
            let hres = require('../../utils/handle_response/handle_response');
            var res_data = hres.handle_response(response.data);
            if (res_data.msg_code == '001') {
              this.setState({
                file: [],
                dialogUpload: false
              })
              this.getAllNode();
            }

            else if (res_data.msg_code == '000') {
              sessionStorage.removeItem('admin_id');
              sessionStorage.removeItem('full_name');
              sessionStorage.removeItem('phone_number');
              sessionStorage.removeItem('email');
              sessionStorage.removeItem('permission');
              sessionStorage.removeItem('username');
              sessionStorage.removeItem('session_token');
              alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
              ReactDOM.render(<LoginPage />, document.getElementById('root'));
            }
            else {
              alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
              this.setState({ dialogLoading: false, file: [], dialogUpload: false });
            }

          })
          .catch(error => {
            console.log(error);
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
            // sessionStorage.removeItem('admin_id');
            // sessionStorage.removeItem('full_name');
            // sessionStorage.removeItem('phone_number');
            // sessionStorage.removeItem('email');
            // sessionStorage.removeItem('permission');
            // sessionStorage.removeItem('username');
            // sessionStorage.removeItem('session_token');
            // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            // ReactDOM.render(<LoginPage />, document.getElementById('root'));
          });
      }
      else {
        alert("File của bạn chưa đúng định dạng!")
      }
    }
  }

  onClickUpdateNode = (event, rowData) => {
    this.setState({
      dialogUpdateNode: true,
      create_description: rowData.DESCRIPTION,
      create_parent_function_id: {
        value: rowData.PARENT_FUNCTION__ID,
        label: rowData.PARENT_FUNCTION__ID
      },
      create_function_id: rowData.FUNCTION_ID,
      old_node: rowData
    });
  }

  onCloseDialogUpdateNode = () => {
    this.setState({
      dialogUpdateNode: false,
      create_parent_function_id: '',
      create_description: '',
      create_function_id: '',
      old_node: null,
      dialogLoading: false,
    });
  }

  onCloseDialogCreateNode = () => {
    this.setState({
      dialogCreateNode: false,
      create_function_id: '',
      create_parent_function_id: '',
      create_description: '',
    });
  }

  createNode = () => {
    this.setState({ dialogCreateNode: true });
  }

  onDrop = (acceptedFiles, rejectedFiles) => {
    this.setState({
      file: acceptedFiles
    });
  }

  onClickUpload = () => {
    this.setState({ dialogUpload: true });
  }

  onCloseDialogUpload = () => {
    this.setState({ dialogUpload: false });
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.dialogUpload}
          //onClose={this.onCloseDialogUploadHandbook}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Tải file</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Hãy đảm bảo file bạn nhập vào đúng định dạng mẫu.  Tên file là: 'node.xlsx'
            </DialogContentText>
            <Dropzone onDrop={this.onDrop}>
              {({ getRootProps, getInputProps, isDragActive }) => {
                return (
                  <div
                    {...getRootProps()}
                    style={{
                      display: 'block',
                      cursor: 'pointer',
                      border: '1px dashed blue',
                      borderRadius: '5px',
                      marginTop: '15px',
                      paddingLeft: '10px',
                      width: '50%',
                      height: '20%'
                    }}
                    className={classNames('dropzone', { 'dropzone--isActive': isDragActive })}
                  >
                    <input {...getInputProps()} />
                    {
                      isDragActive ?
                        <p>Thả file vào đây...</p> :
                        <p>Kéo file vào đây hoặc click để chọn file.</p>
                    }
                  </div>
                )
              }}
            </Dropzone>
            <aside>
              <h2>File đã nhận:</h2>
              <ul>
                {
                  this.state.file.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
                }
              </ul>
            </aside>
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUpload}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickUploadNode}>
              Tải lên
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          // style={{ width: '70%', }}
          open={this.state.dialogCreateNode}
          //onClose={this.onCloseDialogCreateNode}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm lá</DialogTitle>
          <DialogContent style={{
            minHeight: 500,
          }}>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="function_id"
              label='Mã chức năng - Viết in hoa, không dấu, sử dụng ký tự "_" thay dấu cách'
              // type="password"
              id="function_id"
              autoComplete="current-function_id"
              value={this.state.create_function_id}
              onChange={this.handleInputFunctionId}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <br />
            <Typography style={{ marginLeft: '2%', width: '90%' }}>
              Mã chức năng cha:
            </Typography>
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <SelectSearch
                // menuIsOpen={true}
                placeholder="Mã chức năng cha"
                style={{ marginLeft: '2%', width: '60%', color: '#ffffff' }}
                value={this.state.create_parent_function_id}
                onChange={this.handleInputParentFunctionId}
                options={this.state.node_array}
              />
            </FormControl>
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="description"
              label="Mô tả"
              // type="password"
              id="description"
              autoComplete="current-description"
              value={this.state.create_description}
              onChange={this.handleInputDescription}
              style={{ marginLeft: '2%', width: '90%' }}
            />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogCreateNode}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickCreateNode}>
              Tạo
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogUpdateNode}
          //onClose={this.onCloseDialogUpdateNode}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa thông tin</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="category"
              label="Mã chức năng - Viết in hoa, không dấu"
              // type="password"
              id="category"
              autoComplete="current-category"
              value={this.state.create_function_id}
              onChange={this.handleInputFunctionId}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <Typography style={{ marginLeft: '2%', width: '90%' }}>
              Mã chức năng cha:
            </Typography>
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <SelectSearch
                placeholder="Mã chức năng cha"
                style={{ marginLeft: '2%', width: '60%', color: '#ffffff' }}
                value={this.state.create_parent_function_id}
                onChange={this.handleInputParentFunctionId}
                options={this.state.node_array}
              />
            </FormControl>
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="answer"
              label="Mô tả"
              // type="password"
              id="answer"
              autoComplete="current-answer"
              value={this.state.create_description}
              onChange={this.handleInputDescription}
              style={{ marginLeft: '2%', width: '90%' }}
            />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUpdateNode}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateNode}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardActions >
          <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickUpload}>
            Tải lên tệp
          </Button>
          <Button variant="contained" className={classes.buttonFunction}>
            <Link to="/files/node.xlsx" target="_blank" download style={{ color: '#ffffff' }}>
              Tải về file mẫu
            </Link>
          </Button>
        </CardActions>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Danh sách các lá trong cây dữ liệu Chatbot"
            columns={this.state.columns}
            data={this.state.node_array_raw}
            style={{}}
            actions={[
              {
                icon: AddBox,
                tooltip: 'Thêm',
                isFreeAction: true,
                onClick: this.createNode
              },
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdateNode(event, rowData),
              }),
              rowData => ({
                icon: DeleteOutline,
                tooltip: 'Xoá',
                onClick: (event, rowData) => this.onClickDeleteNode(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1, paging: false
            }}
          />
        </CardContent>
      </Card>
    );
  }

  renderTreeView() {
    console.log(this.state.tree)
    return (
      <div style={{ height: 500, marginLeft: '5%' }}>
        <SortableTree
          treeData={this.state.tree}
        />
      </div>

    )
  }

  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <FormControlLabel
          style={{ marginLeft: '2%', marginTop: '2%' }}
          control={
            <Switch
              classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
              }}
              focusVisibleClassName={classes.focusVisible}
              disableRipple
              checked={this.state.showTreeView}
              onChange={event => this.handleChangeTreeView(event)}
              color="primary"
            />
          }
          label="Chế độ xem dạng cây"
        />
        {
          this.state.showTreeView ?
            this.renderTreeView() : this.renderContent()
        }
      </Card>
    )
  }
}

OthersNode.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(OthersNode);