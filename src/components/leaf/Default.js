import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ToggleOn from '@material-ui/icons/ToggleOn';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import Group from '@material-ui/icons/Group';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MuiTree from 'material-ui-tree';
import CardMedia from '@material-ui/core/CardMedia';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SelectSearch from 'react-select';

import Generic_img from '../../assets/generic_sq.png';
import Text_img from '../../assets/text_sq.png';
import List_img from '../../assets/list_sq.png';
import Media_img from '../../assets/media_sq.png';

const tableIcons = {
  Add: AddBox,
  ToggleOn: ToggleOn,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn,
  Group: Group
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  },
  media_: {
    height: 260,
    marginBottom: '2%'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Default extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      showDialogCreate: false,
      showDialogUpdate: false,
      index: null,
      leaf_data: [],
      node_array: [],
      template_id: null,
      create_title: '',
      create_image_url: '',
      create_function_id: '',
      columns: [
        { title: 'Nội dung', field: 'title' },
        { title: 'URL ảnh', field: 'image_url' },
        { title: 'Mã chức năng', field: 'payload' },
      ],
    };
    this.handleInputTitle = this.handleInputTitle.bind(this);
    this.handleInputImageUrl = this.handleInputImageUrl.bind(this);
    this.handleInputFunctionId = this.handleInputFunctionId.bind(this);
  }

  handleInputTitle(event) {
    this.setState({
      create_title: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputImageUrl(event) {
    this.setState({
      create_image_url: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputFunctionId(event) {
    this.setState({
      create_function_id: event
    });
  }

  componentDidMount() {
    this.getLeaf();
  }

  getLeaf() {
    let data = {
      function_id: 'DEFAULT',
    }
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_leaf, hreq._request(data, this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            template_id: res_data.leaf_data[0]._id,
            leaf_data: res_data.leaf_data[0].VALUE.quick_replies,
            dialogLoading: false,
            node_array: [],
          });
          console.log(res_data.leaf_data);
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });
  }

  getNode() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_node_all_, hreq._request('', this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var a = [];
          res_data.node_data.map((e, i) => {
            a.push({
              value: e.FUNCTION_ID,
              label: e.FUNCTION_ID + '- Mô tả: ' + e.DESCRIPTION
            })
          })
          this.setState({
            node_array: a,
            dialogLoading: false
          });
        }

        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });
  }

  updateLeafDefault() {
    this.setState({ dialogLoading: true });
    var array = this.state.leaf_data;
    for (let i = 0; i < array.length; i++) {
      delete array[i].tableData
    }
    let data = {
      template_id: this.state.template_id,
      admin_ID: this.state.admin_id,
      updated_data: {
        VALUE: {
          text: "Một số gợi ý dành cho quý khách",
          quick_replies: this.state.leaf_data
        }
      }
    }
    let hreq = require('../../utils/handle_request/_request');
    axios.put(API.update_default_leaf, hreq._request(data, this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.getLeaf();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });
  }

  updateDefault = () => {
    var array_update = this.state.leaf_data;
    array_update[this.state.index].title = this.state.create_title;
    array_update[this.state.index].image_url = this.state.create_image_url;
    array_update[this.state.index].payload = this.state.create_function_id.value;
    this.setState({
      leaf_data: array_update,
      showDialogUpdate: false,
    })
    this.updateLeafDefault()
  }

  onClickDelete = (event, rowData) => {
    var array = this.state.leaf_data;
    array.splice(rowData.tableData.id);
    this.setState({
      leaf_data: array
    })
    this.updateLeafDefault()
  }

  onClickUpdate = (event, rowData) => {
    var i = rowData.tableData.id;
    this.setState({
      index: i,
      showDialogUpdate: true,
      create_title: this.state.leaf_data[i].title,
      create_image_url: this.state.leaf_data[i].image_url,
      create_function_id: {
        value: this.state.leaf_data[i].payload,
        label: this.state.leaf_data[i].payload,
      }
    })
    this.getNode();
  }

  hideDialogUpdate = () => {
    this.setState({
      showDialogUpdate: false,
      index: null,
      create_title: '',
      create_image_url: '',
      create_function_id: '',
    })
  }

  createDefault = () => {
    var array = this.state.leaf_data;
    array.push({
      content_type: "text",
      title: this.state.create_title,
      image_url: this.state.create_image_url,
      payload: this.state.create_function_id,
    })
    this.setState({
      showDialogCreate: false,
      leaf_data: array
    })
    this.updateLeafDefault()
  }

  showDialogCreate = () => {
    this.setState({
      showDialogCreate: true,
      create_title: '',
      create_image_url: '',
      create_function_id: '',
    })
    this.getNode();
  }
  hideDialogCreate = () => {
    this.setState({
      showDialogCreate: false,
      create_title: '',
      create_image_url: '',
      create_function_id: '',
    })
  }

  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.showDialogCreate}
          //onClose={this.hideDialogCreate}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm nút</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="title"
              label="Nội dung"
              // type="password"
              id="title"
              autoComplete="current-title"
              value={this.state.create_title}
              onChange={this.handleInputTitle}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="image_url"
              label="URL ảnh"
              // type="password"
              id="image_url"
              autoComplete="current-image_url"
              value={this.state.create_image_url}
              onChange={this.handleInputImageUrl}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <SelectSearch
                placeholder="Mã chức năng"
                style={{ marginLeft: '2%', width: '60%', color: '#ffffff' }}
                value={this.state.create_function_id}
                onChange={this.handleInputFunctionId}
                options={this.state.node_array}
              />
            </FormControl>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.hideDialogCreate}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.createDefault}>
              Thêm
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.showDialogUpdate}
          //onClose={this.hideDialogUpdate}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm nút</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="title"
              label="Nội dung"
              // type="password"
              id="title"
              autoComplete="current-title"
              value={this.state.create_title}
              onChange={this.handleInputTitle}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="image_url"
              label="URL ảnh"
              // type="password"
              id="image_url"
              autoComplete="current-image_url"
              value={this.state.create_image_url}
              onChange={this.handleInputImageUrl}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <SelectSearch
                placeholder="Mã chức năng"
                style={{ marginLeft: '2%', width: '60%', color: '#ffffff' }}
                value={this.state.create_function_id}
                onChange={this.handleInputFunctionId}
                options={this.state.node_array}
              />
            </FormControl>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.hideDialogUpdate}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateDefault}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Tin nhắn mặc định"
            columns={this.state.columns}
            data={this.state.leaf_data}
            style={{}}
            actions={[
              {
                icon: AddBox,
                tooltip: 'Thêm',
                isFreeAction: true,
                onClick: this.showDialogCreate
              },
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdate(event, rowData),
              }),
              rowData => ({
                icon: DeleteOutline,
                tooltip: 'Xoá',
                onClick: (event, rowData) => this.onClickDelete(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1, paging: false
            }}
          />
        </CardContent>

      </Card>
    )
  }
}
Default.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Default);