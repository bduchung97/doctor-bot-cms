import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ToggleOn from '@material-ui/icons/ToggleOn';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import Group from '@material-ui/icons/Group';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const tableIcons = {
  Add: AddBox,
  ToggleOn: ToggleOn,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn,
  Group: Group
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  }
});

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      permission: sessionStorage.getItem('permission').split(","),
      admin_id: '',
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      user_id: '',
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      dialogCreateAITrainning: false,
      dialogShowGroup: false,
      dialogUploadAITrainning: false,
      dialogUpdateUser: false,
      user_array_raw: [],
      user_array: [],
      old_user: {
        GROUPS: []
      },
      columns: [
        { title: 'Ảnh Facebook', field: 'url', render: rowData => <img src={rowData.FB_PROFILE_PIC} style={{ width: 50, borderRadius: '50%' }} /> },
        { title: 'Họ tên', field: 'FULLNAME' },
        { title: 'Email', field: 'EMAIL' },
        { title: 'SĐT', field: 'PHONE_NUMBER' },
        { title: 'Giới tính', field: 'FB_GENDER' },
        { title: 'Facebook', field: 'FB_NAME' },
        { title: 'Hoạt động', field: 'IS_ACTIVE' },
      ],
    };
    this.handleInputCreateFullname = this.handleInputCreateFullname.bind(this);
    this.handleInputCreateEmail = this.handleInputCreateEmail.bind(this);
    this.handleInputCreatePhoneNumber = this.handleInputCreatePhoneNumber.bind(this);
  }

  handleInputCreateFullname(event) {
    this.setState({
      fullname: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputCreateEmail(event) {
    this.setState({
      email: event.target.value.replace(/^\s+/g, '').split(' ').join('')
    });
  }
  handleInputCreatePhoneNumber(event) {
    this.setState({
      phonenumber: event.target.value.replace(/^\s+/g, '').split(' ').join('')
    });
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
    this.getAllUser();
  }

  getAllUser() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_all_user, hreq._request('', this.state.session_token))
      .then(response => {
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var data = res_data.users_data;
          for (let i = 0; i < data.length; i++) {
            if (data[i].FB_GENDER == 'male') {
              data[i].FB_GENDER = 'Nam'
            }
            if (data[i].FB_GENDER == 'female') {
              data[i].FB_GENDER = 'Nữ'
            }
            if (data[i].STATUS == 1) {
              data[i].STATUS = 'Đã xác thực'
            }
            if (data[i].STATUS == 0) {
              data[i].STATUS = 'Chưa xác thực'
            }
            if (data[i].IS_ACTIVE == 1) {
              data[i].IS_ACTIVE = 'Đang hoạt động'
            }
            if (data[i].IS_ACTIVE == 0) {
              data[i].IS_ACTIVE = 'Đã chặn'
            }
          }
          this.setState({ user_array: data, dialogLoading: false });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  updateUser = () => {
    if (this.state.fullname == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.email == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    } else if (this.state.phonenumber == '') {
      alert('Bạn chưa nhập đủ thông tin !')
    }
    else {
      this.setState({ dialogLoading: true });
      var data = {}
      data = {
        user_id: this.state.user_id,
        updated_data: {
          FULLNAME: this.state.fullname,
          EMAIL: this.state.email,
          PHONE_NUMBER: this.state.phonenumber
        }
      };
      let hreq = require('../../utils/handle_request/_request');
      console.log(data);
      axios.put(API.update_user, hreq._request(data, this.state.session_token))
        // console.log(data);
        // axios.put(API.update_user, { data: data, session_token: this.state.session_token })
        .then(response => {
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogUpdateUser: false,
              dialogLoading: false
            });
            this.getAllUser();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
          }
        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        });
    }

  }

  onClickUpdateUser = (event, rowData) => {
    this.setState({
      dialogUpdateUser: true,
      fullname: rowData.FULLNAME,
      email: rowData.EMAIL,
      phonenumber: rowData.PHONE_NUMBER,
      user_id: rowData._id
    });
    console.log(rowData)
  }

  onCloseDialogUpdateUser = () => {
    this.setState({
      dialogUpdateUser: false,
      email: '',
      phonenumber: '',
      fullname: '',
    });
  }

  onClickSwitchUser = (event, rowData) => {
    this.setState({ dialogLoading: true });
    var data = {}
    console.log(rowData)
    if (rowData.IS_ACTIVE == 'Đã chặn') {
      data = {
        user_id: rowData._id,
        updated_data: { IS_ACTIVE: 1 }
      };
    }
    if (rowData.IS_ACTIVE == 'Đang hoạt động') {
      data = {
        user_id: rowData._id,
        updated_data: { IS_ACTIVE: 0 }
      };
    }
    let hreq = require('../../utils/handle_request/_request');
    console.log(data);
    axios.put(API.update_user, hreq._request(data, this.state.session_token))
      // console.log(data);
      // axios.put(API.update_user, { data: data, session_token: this.state.session_token })
      .then(response => {
        this.setState({ dialogLoading: false });

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.getAllUser();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  onClickShowGroup = (event, rowData) => {
    this.setState({
      dialogShowGroup: true,
      old_user: rowData
    });
  }

  onCloseDialogShowGroup = () => {
    this.setState({
      dialogShowGroup: false,
      old_user: {
        GROUPS: []
      },
      dialogLoading: false,
    });
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.dialogShowGroup}
          //onClose={this.onCloseDialogShowGroup}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Các nhóm người dùng đang tham gia</DialogTitle>
          <DialogContent>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>STT</TableCell>
                  <TableCell>Tên nhóm</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.old_user.GROUPS.map((e, i) => (
                  <TableRow key={i}>
                    <TableCell component="th" scope="row">
                      {i + 1}
                    </TableCell>
                    <TableCell >{e.NAME}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogShowGroup}>
              Huỷ
            </Button>
          </DialogActions>
        </Dialog>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Dữ liệu người dùng"
            columns={this.state.columns}
            data={this.state.user_array}
            style={{}}
            actions={[
              rowData => ({
                icon: Group,
                tooltip: 'Xem nhóm',
                onClick: (event, rowData) => this.onClickShowGroup(event, rowData),
              }),
              rowData => ({
                icon: ToggleOn,
                tooltip: 'Chặn & Bỏ chặn',
                onClick: (event, rowData) => this.onClickSwitchUser(event, rowData),
              }),
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdateUser(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1, paging: false
            }}
          />
        </CardContent>
      </Card>
    );
  }

  render() {
    if (this.state.permission.indexOf('USER') == -1) {
      return <h2>&emsp;&emsp;&emsp;Bạn không có quyền truy cập chức năng này!</h2>;
    } else {
      const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    Quản lý Người dùng
                  </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Tabs value={this.state.tab_value} textColor="inherit">
              <Tab textColor="inherit" label="Người dùng" />
            </Tabs>
            <Dialog
              open={this.state.dialogUpdateUser}
              aria-labelledby="scroll-dialog-title"
              scroll="paper"
              fullWidth={true}
              maxWidth="xl"
            >
              <DialogTitle id="scroll-dialog-title">Sửa thông tin người dùng</DialogTitle>
              <DialogContent>
                <TextField
                  //variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="fullname"
                  label="Họ tên"
                  id="fullname"
                  autoComplete="fullname"
                  value={this.state.fullname}
                  onChange={this.handleInputCreateFullname}
                  style={{ marginLeft: '2%', width: '90%' }}
                />
                <br />
                <TextField
                  //variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="email"
                  label="Email"
                  id="email"
                  autoComplete="email"
                  value={this.state.email}
                  onChange={this.handleInputCreateEmail}
                  style={{ marginLeft: '2%', width: '90%' }}
                />
                <br />
                <TextField
                  //variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="phonenumber"
                  label="Số điện thoại"
                  id="phonenumber"
                  autoComplete="phonenumber"
                  value={this.state.phonenumber}
                  onChange={this.handleInputCreatePhoneNumber}
                  style={{ marginLeft: '2%', width: '90%' }}
                />
              </DialogContent>
              <DialogActions style={{ marginRight: '40%' }}>
                <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUpdateUser}>
                  Huỷ
            </Button>
                <Button variant="contained" className={classes.buttonFunction} onClick={this.updateUser}>
                  Cập nhật
            </Button>
              </DialogActions>
            </Dialog>
          </AppBar>
          {this.state.tab_value === 0 && this.renderContent()}
        </React.Fragment>
      );
    }
  }
}

User.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(User);