import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const tableIcons = {
  Add: AddBox,
  Check: Check,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  }
});

class Group extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      permission: sessionStorage.getItem('permission').split(","),
      admin_id: '',
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      dialogCreateGroup: false,
      dialogUpdateGroup: false,
      dialogUploadAITrainning: false,
      group_array: [],
      user_array: [],
      file: [],
      old_group: { USERS: [] },
      columns: [
        { title: 'Tên nhóm', field: 'NAME' },
      ],
      create_group_name: '',
      create_member_list: [],
      selected_user: [],
      showAddUserToGroup: false,
      showRemoveUserFromGroup: true,
      data_4_delete: null,
      data_4_add: null,
    };
    this.handleInputGroupName = this.handleInputGroupName.bind(this);

  }

  handleInputGroupName(event) {
    this.setState({
      create_group_name: event.target.value.replace(/^\s+/g, '')
    });
  }

  componentWillMount() {
    this.getAllGroup();
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
  }

  getAllGroup() {
    this.setState({ dialogLoading: true })
    // axios.post(API.show_all_group, { data: '', session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_all_group, hreq._request('', this.state.session_token))
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            group_array: res_data.groups_data,
            dialogCreateGroup: false,
            dialogUpdateGroup: false,
            dialogUploadAITrainning: false,
            user_array: [],
            file: [],
            old_group: { USERS: [] },
            columns: [
              { title: 'Tên nhóm', field: 'NAME' },
            ],
            create_group_name: '',
            create_member_list: [],
            selected_user: [],
            showAddUserToGroup: false,
            showRemoveUserFromGroup: true,
            data_4_delete: null,
            data_4_add: null,
          });
          console.log(res_data.groups_data)
          this.setState({ dialogLoading: false });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  getAllUser() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_user_create_group, hreq._request('', this.state.session_token))
      // axios.post(API.show_user_create_group, { data: '', session_token: this.state.session_token })
      .then(response => {
        
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
         
          this.setState({ user_array: res_data.users_data });
          this.setState({ dialogLoading: false });
          console.log(res_data.users_data );
        }
        
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  getAllUserForAddUser() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_user_create_group, hreq._request('', this.state.session_token))
      // axios.post(API.show_user_create_group, { data: '', session_token: this.state.session_token })
      .then(response => {

        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          console.log(res_data.users_data)
          console.log(this.state.old_group.USERS);
          if (this.state.old_group.USERS.length == 0) {
            this.setState({ user_array: res_data.users_data });
          } else {
            for (let i = 0; i < this.state.old_group.USERS.length; i++) {
              let dofoa = require('../../utils/delete_object_from_object_array');
              console.log(dofoa.delete_object_from_object_array(res_data.users_data, this.state.old_group.USERS[i].USER_ID))
              this.setState({ user_array: dofoa.delete_object_from_object_array(res_data.users_data, this.state.old_group.USERS[i].USER_ID) });
            }
          }

          this.setState({ dialogLoading: false });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  onClickDeleteGroup = (event, rowData) => {
    this.setState({ dialogLoading: true });
    let data = {
      group_ID: rowData.GROUP_ID,
    };
    let hreq = require('../../utils/handle_request/_request');
    console.log(data);
    axios.post(API.delete_group, hreq._request(data, this.state.session_token))
      // axios.post(API.delete_group, { data: data, session_token: this.state.session_token })
      .then(response => {
        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.getAllGroup();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  updateGroup = () => {
    this.setState({ dialogLoading: true });

    if (this.state.data_4_add != null) {
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.add_member_group, hreq._request(this.state.data_4_add, this.state.session_token))
        // axios.post(API.delete_group, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogUpdateGroup: false,
              showAddUserToGroup: false,
              showRemoveUserFromGroup: true,
              old_group: { USERS: [] },
            });
            this.getAllGroup();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });
    }

    if (this.state.data_4_delete != null) {
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.remove_member_group, hreq._request(this.state.data_4_delete, this.state.session_token))
        // axios.post(API.delete_group, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogUpdateGroup: false,
              old_group: { USERS: [] },
            });
            this.getAllGroup();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });
    }

    let data = {
      group_ID: this.state.old_group.GROUP_ID,
      updated_data: {
        // NAME: this.state.create_group_name,
        UPDATED_BY: this.state.admin_id,
        UPDATED_AT: new Date()
      }
    }
    if (this.state.old_group.NAME !== this.state.create_group_name) {
      Object.assign(data.updated_data, { NAME: this.state.create_group_name })
    }
    console.log(data);
    let hreq = require('../../utils/handle_request/_request');
    axios.put(API.update_group, hreq._request(data, this.state.session_token))
      // axios.put(API.update_ai_trainning, { data: data, session_token: this.state.session_token })
      .then(response => {
        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            dialogUpdateGroup: false,
            create_member_list: [],
            create_group_name: '',
            old_group: { USERS: [] },
          });
          this.getAllGroup();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Tên nhóm đã tồn tại!');
          this.setState({
            dialogUpdateGroup: false,
            create_member_list: [],
            create_group_name: '',
            old_group: { USERS: [] },
            dialogLoading: false,
          })
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });



  }

  onClickCreateGroup = () => {
    if (this.state.create_group_name == '') {
      alert('Bạn chưa điền đủ thông tin!')
    } else {
      this.setState({ dialogLoading: true });
      var member_list = []
      for (let i = 0; i < this.state.selected_user.length; i++) {
        member_list.push({ user_ID: this.state.selected_user[i]._id, PSID: this.state.selected_user[i].PSID })
      }
      var data = {
        group_name: this.state.create_group_name,
        member_list: member_list,
        admin_id: this.state.admin_id,
      }
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.create_group, hreq._request(data, this.state.session_token))
        // axios.post(API.create_group, { data: data, session_token: this.state.session_token })
        .then(response => {

          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          // var res_data = response.data;
          if (res_data.msg_code == '001') {
            this.setState({
              create_group_name: '',
              create_member_list: [],
              dialogCreateGroup: false,
            })
            this.getAllGroup();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Tên nhóm bị trùng!');
            this.setState({
              create_group_name: '',
              create_member_list: [],
              dialogCreateGroup: false,
            })
            this.setState({ dialogLoading: false });
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });
    }
  }

  onClickRemoveMember = (evt, _data) => {
    // this.setState({ dialogLoading: true });
    console.log(_data)
    var user_list = [],
      array = this.state.old_group.USERS;
    for (let i = 0; i < _data.length; i++) {
      user_list.push(_data[i].USER_ID);
      delete array[_data[i].tableData.id]
    }
    array = array.filter(x => !!x)
    let data = {
      group_ID: this.state.old_group.GROUP_ID,
      user_list: user_list
    };
    // console.log(data);
    this.state.old_group.USERS = array;
    this.setState({ data_4_delete: data });


    // let hreq = require('../../utils/handle_request/_request');
    // axios.post(API.remove_member_group, hreq._request(data, this.state.session_token))
    //   // axios.post(API.delete_group, { data: data, session_token: this.state.session_token })
    //   .then(response => {

    //     // var res_data = response.data;
    //     let hres = require('../../utils/handle_response/handle_response');
    //     var res_data = hres.handle_response(response.data);
    //     if (res_data.msg_code == '001') {
    //       this.setState({
    //         dialogUpdateGroup: false,
    //         old_group: { USERS: [] },
    //       });
    //       this.getAllGroup();
    //     }
    //     else if (res_data.msg_code == '000') {
    //       sessionStorage.removeItem('admin_id');
    //       sessionStorage.removeItem('full_name');
    //       sessionStorage.removeItem('phone_number');
    //       sessionStorage.removeItem('email');
    //       sessionStorage.removeItem('permission');
    //       sessionStorage.removeItem('username');
    //       sessionStorage.removeItem('session_token');
    //       alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
    //       ReactDOM.render(<LoginPage />, document.getElementById('root'));
    //     }
    //     else {
    //       alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
    //       this.setState({ dialogLoading: false });
    //     }

    //   })
    //   .catch(error => {
    //     console.log(error);
    //     alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
    //     this.setState({ dialogLoading: false });
    //     // sessionStorage.removeItem('admin_id');
    //     // sessionStorage.removeItem('full_name');
    //     // sessionStorage.removeItem('phone_number');
    //     // sessionStorage.removeItem('email');
    //     // sessionStorage.removeItem('permission');
    //     // sessionStorage.removeItem('username');
    //     // sessionStorage.removeItem('session_token');
    //     // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
    //     // ReactDOM.render(<LoginPage />, document.getElementById('root'));
    //   });
  }

  onClickAddMember = (evt, _data) => {
    // this.setState({ dialogLoading: true });
    console.log(_data)
    var user_list = [];
    for (let i = 0; i < _data.length; i++) {
      user_list.push({ user_ID: _data[i]._id, PSID: _data[i].PSID, FULLNAME: _data[i].FULLNAME, EMAIL: _data[i].EMAIL });
    }
    let data = {
      group_ID: this.state.old_group.GROUP_ID,
      user_list: user_list
    };

    this.setState({
      data_4_add: data,
      showAddUserToGroup: false,
      showRemoveUserFromGroup: true
    });
    var array = this.state.old_group.USERS.concat(user_list);
    this.state.old_group.USERS = array;
    // let hreq = require('../../utils/handle_request/_request');
    // console.log(data);
    // axios.post(API.add_member_group, hreq._request(data, this.state.session_token))
    //   // axios.post(API.delete_group, { data: data, session_token: this.state.session_token })
    //   .then(response => {

    //     // var res_data = response.data;
    //     let hres = require('../../utils/handle_response/handle_response');
    //     var res_data = hres.handle_response(response.data);
    //     if (res_data.msg_code == '001') {
    //       this.setState({
    //         dialogUpdateGroup: false,
    //         showAddUserToGroup: false,
    //         showRemoveUserFromGroup: true,
    //         old_group: { USERS: [] },
    //       });
    //       this.getAllGroup();
    //     }
    //     else if (res_data.msg_code == '000') {
    //       sessionStorage.removeItem('admin_id');
    //       sessionStorage.removeItem('full_name');
    //       sessionStorage.removeItem('phone_number');
    //       sessionStorage.removeItem('email');
    //       sessionStorage.removeItem('permission');
    //       sessionStorage.removeItem('username');
    //       sessionStorage.removeItem('session_token');
    //       alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
    //       ReactDOM.render(<LoginPage />, document.getElementById('root'));
    //     }
    //     else {
    //       alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
    //       this.setState({ dialogLoading: false });
    //     }

    //   })
    //   .catch(error => {
    //     console.log(error);
    //     alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
    //     this.setState({ dialogLoading: false });
    //     // sessionStorage.removeItem('admin_id');
    //     // sessionStorage.removeItem('full_name');
    //     // sessionStorage.removeItem('phone_number');
    //     // sessionStorage.removeItem('email');
    //     // sessionStorage.removeItem('permission');
    //     // sessionStorage.removeItem('username');
    //     // sessionStorage.removeItem('session_token');
    //     // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
    //     // ReactDOM.render(<LoginPage />, document.getElementById('root'));
    //   });
  }

  onClickUpdateGroup = (event, rowData) => {
    rowData.USERS.map((e, i) => {
      console.log(rowData.USERS)
      if (e.IS_ACTIVE == 1) {
        e.IS_ACTIVE = 'Đang hoạt động'
      } else {
        e.IS_ACTIVE = 'Đã chặn'
      }
    })
    this.setState({
      dialogUpdateGroup: true,
      create_group_name: rowData.NAME,
      old_group: rowData
    });
  }

  onCloseDialogUpdateGroup = () => {
    this.setState({
      dialogUpdateGroup: false,
      create_group_name: '',
      old_group: { USERS: [] },
      dialogLoading: false,
      showAddUserToGroup: false,
      showRemoveUserFromGroup: true,
      data_4_add: null,
      data_4_delete: null,
    });
    // this.getAllGroup()
  }

  onCloseDialogCreateGroup = () => {
    this.setState({ dialogCreateGroup: false, selected_user: [] });
    // this.getAllGroup()
  }

  createGroup = (evt, data) => {
    this.setState({ dialogLoading: true, dialogCreateGroup: true });
    this.getAllUser();
  }

  openAddMember = () => {
    this.setState({ dialogLoading: true, showAddUserToGroup: true, showRemoveUserFromGroup: false });
    this.getAllUserForAddUser();
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.dialogCreateGroup}
          //onClose={this.onCloseDialogCreateGroup}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm nhóm</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="group_name"
              label="Tên nhóm"
              // type="password"
              id="group_name"
              autoComplete="current-function_id"
              value={this.state.create_group_name}
              onChange={this.handleInputGroupName}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <MaterialTable
              icons={tableIcons}
              title="Thêm học viên vào nhóm"
              columns={[
                { title: 'Tên học viên', field: 'FULLNAME' },
                { title: 'Email', field: 'EMAIL' },
              ]}
              data={this.state.user_array}
              options={{
                selection: true
              }}
              onSelectionChange={(rows) => this.setState({ selected_user: rows })}
            />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogCreateGroup}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickCreateGroup}>
              Tạo
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogUpdateGroup}
          //onClose={this.onCloseDialogUpdateGroup}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa nhóm</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="group_name"
              label="Tên nhóm"
              // type="password"
              id="group_name"
              autoComplete="current-function_id"
              value={this.state.create_group_name}
              onChange={this.handleInputGroupName}
              style={{ marginLeft: '2%', width: '90%' }}
            />
          </DialogContent>
          <DialogContent>
            {this.state.showRemoveUserFromGroup ? <MaterialTable
              icons={tableIcons}
              title="Chọn để xoá học viên khỏi nhóm"
              columns={[
                { title: 'Tên học viên', field: 'FULLNAME' },
                { title: 'Email', field: 'EMAIL' },
                { title: 'Trạng thái hoạt động', field: 'IS_ACTIVE' },
              ]}
              data={this.state.old_group.USERS}
              options={{
                selection: true
              }}
              actions={[
                {
                  icon: AddBox,
                  tooltip: 'Thêm',
                  isFreeAction: true,
                  onClick: this.openAddMember
                },
                {
                  tooltip: 'Xác nhận xoá',
                  icon: DeleteOutline,
                  onClick: (evt, data) => this.onClickRemoveMember(evt, data)
                }
              ]}
            /> : null}
            {this.state.showAddUserToGroup ? <MaterialTable
              icons={tableIcons}
              title="Chọn học viên để thêm vào nhóm"
              columns={[
                { title: 'Tên học viên', field: 'FULLNAME' },
                { title: 'Email', field: 'EMAIL' },
              ]}
              data={this.state.user_array}
              options={{
                selection: true
              }}
              actions={[
                {
                  tooltip: 'Xác nhận',
                  icon: Check,
                  onClick: (evt, data) => this.onClickAddMember(evt, data)
                }
              ]}
            /> : null}
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUpdateGroup}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateGroup}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Danh sách Nhóm"
            columns={this.state.columns}
            data={this.state.group_array}
            style={{}}
            actions={[
              {
                icon: AddBox,
                tooltip: 'Thêm',
                isFreeAction: true,
                onClick: this.createGroup
              },
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdateGroup(event, rowData),
              }),
              rowData => ({
                icon: DeleteOutline,
                tooltip: 'Xoá',
                onClick: (event, rowData) => this.onClickDeleteGroup(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1, paging: false
            }}
          />
        </CardContent>
      </Card>
    );
  }

  render() {
    if (this.state.permission.indexOf('GROU') == -1) {
      return <h2>&emsp;&emsp;&emsp;Bạn không có quyền truy cập chức năng này!</h2>;
    } else {
      console.log(this.state.group_array);
      const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    Quản lý Nhóm lớp học
                </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Tabs value={this.state.tab_value} textColor="inherit">
              <Tab textColor="inherit" label="Danh sách Nhóm" />
            </Tabs>
          </AppBar>
          {this.state.tab_value === 0 && this.renderContent()}
        </React.Fragment>
      );
    }
  }
}

Group.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Group);