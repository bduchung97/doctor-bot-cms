import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const tableIcons = {
  Add: AddBox,
  Check: Check,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  }
});

class HandBook extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      permission: sessionStorage.getItem('permission').split(","),
      admin_id: '',
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      dialogCreateHandbook: false,
      dialogUpdateHandbook: false,
      dialogUploadHandbook: false,
      handbook_array: [],
      file: [],
      old_handbook: null,
      columns: [

        {
          title: 'Câu hỏi', field: 'QUESTION',
          cellStyle: {
            width: 300
          },
        },
        { title: 'Câu trả lời', field: 'ANSWER' },
      ],
      create_category: '',
      create_question: '',
      create_answer: '',
    };
    this.handleInputCategory = this.handleInputCategory.bind(this);
    this.handleInputCreateQuestion = this.handleInputCreateQuestion.bind(this);
    this.handleInputCreateAnswer = this.handleInputCreateAnswer.bind(this);
  }

  handleInputCategory(event) {
    this.setState({
      create_category: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputCreateQuestion(event) {
    this.setState({
      create_question: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputCreateAnswer(event) {
    this.setState({
      create_answer: event.target.value.replace(/^\s+/g, '')
    });
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
    this.getAllHandbook();
  }

  getAllHandbook() {
    // axios.post(API.show_all_handbook, { data: '', session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_all_handbook, hreq._request('', this.state.session_token))
      .then(response => {
        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({ handbook_array: res_data.handbooks_data });
          this.setState({
            dialogLoading: false,
            dialogCreateHandbook: false,
            dialogUpdateHandbook: false,
            dialogUploadHandbook: false,
            file: [],
            old_handbook: null,
            columns: [
              {
                title: 'Câu hỏi', field: 'QUESTION',
                cellStyle: {
                  width: 300
                },
              },
              { title: 'Câu trả lời', field: 'ANSWER' },
            ],
            create_category: '',
            create_question: '',
            create_answer: '',
          });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  onClickUploadHandbook = () => {
    if (!this.state.file[0]) {
      alert("Bạn chưa chọn file!")
    }
    else {
      if (this.state.file[0].name === 'handbook.xlsx') {
        this.setState({ dialogLoading: true });
        let formData = new FormData();
        formData.append('session_token', this.state.session_token);
        formData.append('file', this.state.file[0]);
        formData.append('file_name', this.state.file[0].name);
        formData.append('admin_id', this.state.admin_id);
        axios.post(API.upload_handbook, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
          .then(response => {

            // var res_data = response.data;
            let hres = require('../../utils/handle_response/handle_response');
            var res_data = hres.handle_response(response.data);
            if (res_data.msg_code == '001') {
              this.setState({
                file: [],
                dialogUploadHandbook: false
              })
              this.getAllHandbook();
            }

            else if (res_data.msg_code == '000') {
              sessionStorage.removeItem('admin_id');
              sessionStorage.removeItem('full_name');
              sessionStorage.removeItem('phone_number');
              sessionStorage.removeItem('email');
              sessionStorage.removeItem('permission');
              sessionStorage.removeItem('username');
              sessionStorage.removeItem('session_token');
              alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
              ReactDOM.render(<LoginPage />, document.getElementById('root'));
            }
            else {
              alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
              this.setState({ dialogLoading: false, file: [], dialogUploadHandbook: false });
            }

          })
          .catch(error => {
            console.log(error);
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
            // sessionStorage.removeItem('admin_id');
            // sessionStorage.removeItem('full_name');
            // sessionStorage.removeItem('phone_number');
            // sessionStorage.removeItem('email');
            // sessionStorage.removeItem('permission');
            // sessionStorage.removeItem('username');
            // sessionStorage.removeItem('session_token');
            // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            // ReactDOM.render(<LoginPage />, document.getElementById('root'));
          });
      }
      else {
        alert("File của bạn chưa đúng định dạng!")
      }
    }
  }

  onClickDeleteHandbook = (event, rowData) => {
    this.setState({ dialogLoading: true });
    let data = {
      handbook_id: rowData._id,
    };
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.delete_handbook, hreq._request(data, this.state.session_token))
      // axios.post(API.delete_handbook, { data: data, session_token: this.state.session_token })
      .then(response => {
        this.setState({ dialogLoading: false });

        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        // var res_data = response.data;
        if (res_data.msg_code == '001') {
          this.getAllHandbook();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  updateHandbook = () => {
    if (this.state.create_question == '') {
      alert('Bạn chưa điền đủ thông tin!')
    }
    else if (this.state.create_answer == '') {
      alert('Bạn chưa điền đủ thông tin!')
    }
    else {
      this.setState({ dialogLoading: true });

      let data = {
        handbook_id: this.state.old_handbook._id,
        updated_data: {
          QUESTION: this.state.create_question,
          ANSWER: this.state.create_answer,
          CATEGORY: this.state.create_category,
          UPDATED_BY: this.state.admin_id,
          UPDATED_AT: new Date()
        }
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.put(API.update_handbook, hreq._request(data, this.state.session_token))
        // axios.put(API.update_handbook, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogUpdateHandbook: false,
              create_question: '',
              create_answer: '',
              create_category: '',
              old_handbook: null,
            });
            this.getAllHandbook();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({
              dialogUpdateHandbook: false,
              create_question: '',
              create_answer: '',
              create_category: '',
              old_handbook: null,
              dialogLoading: false,
            })
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });

    }
  }

  onClickCreateHandbook = () => {
    if (this.state.create_question == '') {
      alert('Bạn chưa điền đủ thông tin!')
    }
    else if (this.state.create_answer == '') {
      alert('Bạn chưa điền đủ thông tin!')
    }
    else {
      this.setState({ dialogLoading: true });
      let data = {
        category: this.state.create_category,
        question: this.state.create_question,
        answer: this.state.create_answer,
        admin_id: this.state.admin_id,
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.create_handbook, hreq._request(data, this.state.session_token))
        // axios.post(API.create_handbook, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              create_category: '',
              create_answer: '',
              create_question: '',
              dialogCreateHandbook: false,
            })
            this.getAllHandbook();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({
              create_category: '',
              create_answer: '',
              create_question: '',
              dialogCreateHandbook: false,
            })
            this.setState({ dialogLoading: false });
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });
    }
  }

  onClickUpdateHandbook = (event, rowData) => {
    this.setState({
      dialogUpdateHandbook: true,
      create_answer: rowData.ANSWER,
      create_question: rowData.QUESTION,
      create_category: rowData.CATEGORY,
      old_handbook: rowData
    });
  }

  onCloseDialogUpdateHandbook = () => {
    this.setState({
      dialogUpdateHandbook: false,
      create_question: '',
      create_answer: '',
      create_category: '',
      old_handbook: null,
      dialogLoading: false,
    });
  }

  onCloseDialogCreateHandbook = () => {
    this.setState({ dialogCreateHandbook: false, create_answer: '', create_question: '', });
    // this.getAllHandbook();
  }

  createHandbook = () => {
    this.setState({ dialogCreateHandbook: true });
  }

  onCloseDialogUploadHandbook = () => {
    this.setState({ dialogUploadHandbook: false, create_answer: '', create_question: '', });
    // this.getAllHandbook();
  }

  onClickUpload = () => {
    this.setState({ dialogUploadHandbook: true });
  }

  onDrop = (acceptedFiles, rejectedFiles) => {
    this.setState({
      file: acceptedFiles
    });
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.dialogUploadHandbook}
          //onClose={this.onCloseDialogUploadHandbook}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Tải file</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Hãy đảm bảo file bạn nhập vào đúng định dạng mẫu.  Tên file là: 'handbook.xlsx'
            </DialogContentText>
            <Dropzone onDrop={this.onDrop}>
              {({ getRootProps, getInputProps, isDragActive }) => {
                return (
                  <div
                    {...getRootProps()}
                    style={{
                      display: 'block',
                      cursor: 'pointer',
                      border: '1px dashed blue',
                      borderRadius: '5px',
                      marginTop: '15px',
                      paddingLeft: '10px',
                      width: '50%',
                      height: '20%'
                    }}
                    className={classNames('dropzone', { 'dropzone--isActive': isDragActive })}
                  >
                    <input {...getInputProps()} />
                    {
                      isDragActive ?
                        <p>Thả file vào đây...</p> :
                        <p>Kéo file vào đây hoặc click để chọn file.</p>
                    }
                  </div>
                )
              }}
            </Dropzone>
            <aside>
              <h2>File đã nhận:</h2>
              <ul>
                {
                  this.state.file.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
                }
              </ul>
            </aside>
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUploadHandbook}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickUploadHandbook}>
              Tải lên
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogCreateHandbook}
          //onClose={this.onCloseDialogCreateHandbook}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm dữ liệu vào sổ tay</DialogTitle>
          <DialogContent>
            {/* <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="category"
              label="Thể loại"
              // type="password"
              id="category"
              autoComplete="current-category"
              value={this.state.create_category}
              onChange={this.handleInputCategory}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br /> */}
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="question"
              label="Câu hỏi"
              // type="password"
              id="question"
              autoComplete="current-question"
              value={this.state.create_question}
              onChange={this.handleInputCreateQuestion}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="answer"
              label="Câu trả lời"
              // type="password"
              id="answer"
              autoComplete="current-answer"
              value={this.state.create_answer}
              onChange={this.handleInputCreateAnswer}
              style={{ marginLeft: '2%', width: '90%' }}
            />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogCreateHandbook}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickCreateHandbook}>
              Tạo
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogUpdateHandbook}
          //onClose={this.onCloseDialogUpdateHandbook}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa thông tin</DialogTitle>
          <DialogContent>
            {/* <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="category"
              label="Thể loại"
              // type="password"
              id="category"
              autoComplete="current-category"
              value={this.state.create_category}
              onChange={this.handleInputCategory}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br /> */}
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="question"
              label="Câu hỏi"
              // type="password"
              id="question"
              autoComplete="current-question"
              value={this.state.create_question}
              onChange={this.handleInputCreateQuestion}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="answer"
              label="Câu trả lời"
              // type="password"
              id="answer"
              autoComplete="current-answer"
              value={this.state.create_answer}
              onChange={this.handleInputCreateAnswer}
              style={{ marginLeft: '2%', width: '90%' }}
            />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUpdateHandbook}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateHandbook}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardActions >
          <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickUpload}>
            Tải lên tệp
          </Button>
          <Button variant="contained" className={classes.buttonFunction}>
            <Link to="/files/handbook.xlsx" target="_blank" download style={{ color: '#ffffff' }}>
              Tải về file mẫu
            </Link>
          </Button>
        </CardActions>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Cẩm nang tư vấn"
            columns={this.state.columns}
            data={this.state.handbook_array}
            style={{}}
            actions={[
              {
                icon: AddBox,
                tooltip: 'Thêm',
                isFreeAction: true,
                onClick: this.createHandbook
              },
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdateHandbook(event, rowData),
              }),
              rowData => ({
                icon: DeleteOutline,
                tooltip: 'Xoá',
                onClick: (event, rowData) => this.onClickDeleteHandbook(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1, paging: false
            }}
          />
        </CardContent>
      </Card>
    );
  }

  render() {
    if (this.state.permission.indexOf('HAND') == -1) {
      return <h2>&emsp;&emsp;&emsp;Bạn không có quyền truy cập chức năng này!</h2>;
    } else {
      console.log(this.state.handbook_array);
      const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    Quản lý Cẩm nang tư vấn
                </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Tabs value={this.state.tab_value} textColor="inherit">
              <Tab textColor="inherit" label="Cẩm nang tư vấn" />
            </Tabs>
          </AppBar>
          {this.state.tab_value === 0 && this.renderContent()}
        </React.Fragment>
      );
    }
  }
}

HandBook.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(HandBook);