import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SelectSearch from 'react-select';

const tableIcons = {
  Add: AddBox,
  Check: Check,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  }
});

class AITrainning extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      permission: sessionStorage.getItem('permission').split(","),
      admin_id: '',
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      dialogCreateAITrainning: false,
      dialogUpdateAITrainning: false,
      dialogUploadAITrainning: false,
      dialogApprove: false,
      ai_trainning_array: [],
      ai_trainning_approve_array: [],
      file: [],
      old_ai_trainning: null,
      columns: [
        { title: 'Mã chức năng', field: 'FUNCTION_ID' },
        { title: 'Dữ liệu huẩn luyện', field: 'VALUE' },
      ],
      create_function_id: '',
      create_value: '',
      create_answer: '',
      node_array: [],
    };
    this.handleInputFunctionID = this.handleInputFunctionID.bind(this);
    this.handleInputCreateValue = this.handleInputCreateValue.bind(this);
    this.handleInputCreateAnswer = this.handleInputCreateAnswer.bind(this);
  }

  handleInputFunctionID(event) {
    this.setState({
      create_function_id: event
    });
  }
  handleInputCreateValue(event) {
    this.setState({
      create_value: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputCreateAnswer(event) {
    this.setState({
      create_answer: event.target.value.replace(/^\s+/g, '')
    });
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
    this.getAllAITrainning();
  }

  getAllAITrainning() {
    // axios.post(API.show_all_ai_trainning, { data: '', session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_all_ai_trainning, hreq._request('', this.state.session_token))
      .then(response => {
        console.log(response);
        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            ai_trainning_array: res_data.ai_trainning_data,
            dialogCreateAITrainning: false,
            dialogUpdateAITrainning: false,
            dialogUploadAITrainning: false,
            dialogApprove: false,
            ai_trainning_approve_array: [],
            file: [],
            old_ai_trainning: null,
            create_function_id: '',
            create_value: '',
            create_answer: '',
            node_array: [],
          });
          this.setState({ dialogLoading: false });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }
  getApproveAITrainning() {
    // axios.post(API.show_all_ai_trainning, { data: '', session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_approve_ai_trainning, hreq._request('', this.state.session_token))
      .then(response => {
        console.log(response);
        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({ ai_trainning_approve_array: res_data.ai_trainning_data });
          this.setState({ dialogLoading: false });
        }

        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  getNode() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_node_all_, hreq._request('', this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var a = [];
          res_data.node_data.map((e, i) => {
            a.push({
              value: e.FUNCTION_ID,
              label: e.FUNCTION_ID + '- Mô tả: ' + e.DESCRIPTION
            })
          })
          this.setState({
            node_array: a,
            dialogLoading: false
          });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  onClickUploadAITrainning = () => {
    if (!this.state.file[0]) {
      alert("Bạn chưa chọn file!")
    }
    else {
      if (this.state.file[0].name === 'ai_trainning.xlsx') {
        this.setState({ dialogLoading: true });
        let formData = new FormData();
        formData.append('session_token', this.state.session_token);
        formData.append('file', this.state.file[0]);
        formData.append('file_name', this.state.file[0].name);
        formData.append('admin_id', this.state.admin_id);
        axios.post(API.upload_ai_trainning, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
          .then(response => {
            // var res_data = response.data;
            let hres = require('../../utils/handle_response/handle_response');
            var res_data = hres.handle_response(response.data);
            if (res_data.msg_code == '001') {
              this.setState({
                file: [],
                dialogUploadAITrainning: false
              })
              this.getAllAITrainning();
            }
            else if (res_data.msg_code == '000') {
              sessionStorage.removeItem('admin_id');
              sessionStorage.removeItem('full_name');
              sessionStorage.removeItem('phone_number');
              sessionStorage.removeItem('email');
              sessionStorage.removeItem('permission');
              sessionStorage.removeItem('username');
              sessionStorage.removeItem('session_token');
              alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
              ReactDOM.render(<LoginPage />, document.getElementById('root'));
            }
            else {
              alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
              this.setState({ dialogLoading: false, file: [], dialogUploadAITrainning: false });
            }
          })
          .catch(error => {
            console.log(error);
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
            // sessionStorage.removeItem('admin_id');
            // sessionStorage.removeItem('full_name');
            // sessionStorage.removeItem('phone_number');
            // sessionStorage.removeItem('email');
            // sessionStorage.removeItem('permission');
            // sessionStorage.removeItem('username');
            // sessionStorage.removeItem('session_token');
            // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            // ReactDOM.render(<LoginPage />, document.getElementById('root'));
          });
      }
      else {
        alert("File của bạn chưa đúng định dạng!")
      }
    }
  }

  onClickDeleteAITrainning = (event, rowData) => {
    this.setState({ dialogLoading: true });

    let data = {
      ai_trainning_id: rowData._id,
    };
    // let hreq = require('../../utils/handle_request/_request');
    // axios.post(API.delete_ai_trainning, { data: data, session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.delete_ai_trainning, hreq._request(data, this.state.session_token))
      .then(response => {
        this.setState({ dialogLoading: false });

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.getAllAITrainning();
          this.getApproveAITrainning();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }
  onClickApproveAI = (event, rowData) => {
    this.setState({ dialogLoading: true });

    let data = {
      ai_trainning_id: rowData._id,
    };
    // let hreq = require('../../utils/handle_request/_request');
    // axios.post(API.delete_ai_trainning, { data: data, session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.put(API.approve_ai_trainning, hreq._request(data, this.state.session_token))
      .then(response => {
        this.setState({ dialogLoading: false });

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.getApproveAITrainning();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  updateAITrainning = () => {
    if (this.state.create_value == '') {
      alert('Bạn chưa nhập đủ thông tin!')
    }
    else if (this.state.create_function_id.value == '') {
      alert('Bạn chưa nhập đủ thông tin!')
    }
    else {
      this.setState({ dialogLoading: true });

      let data = {
        ai_trainning_id: this.state.old_ai_trainning._id,
        updated_data: {
          VALUE: this.state.create_value,
          FUNCTION_ID: this.state.create_function_id.value,
          UPDATED_BY: this.state.admin_id,
          UPDATED_AT: new Date()
        }
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.put(API.update_ai_trainning, hreq._request(data, this.state.session_token))
        // axios.put(API.update_ai_trainning, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogUpdateAITrainning: false,
              create_value: '',
              create_function_id: '',
              old_ai_trainning: null,
            });
            this.getAllAITrainning();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({
              dialogUpdateAITrainning: false,
              create_value: '',
              create_function_id: '',
              old_ai_trainning: null,
              dialogLoading: false,
            })
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });

    }
  }

  onClickCreateAITrainning = () => {
    console.log(this.state.create_function_id.value);
    if (this.state.create_value == '') {
      alert('Bạn chưa nhập đủ thông tin!')
    }
    else if (!this.state.create_function_id.value) {
      alert('Bạn chưa nhập đủ thông tin!')
    }
    else {
      this.setState({ dialogLoading: true });
      let data = {
        function_id: this.state.create_function_id.value,
        value: this.state.create_value,
        admin_id: this.state.admin_id,
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.create_ai_trainning, hreq._request(data, this.state.session_token))
        // axios.post(API.create_ai_trainning, { data: data, session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              create_function_id: '',
              create_value: '',
              dialogCreateAITrainning: false,
            })
            this.getAllAITrainning();
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({
              create_function_id: '',
              create_value: '',
              dialogCreateAITrainning: false,
            })
            this.setState({ dialogLoading: false });
          }

        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });
    }
  }

  onClickUpdateAITrainning = (event, rowData) => {
    this.getNode();
    this.setState({
      dialogUpdateAITrainning: true,
      create_value: rowData.VALUE,
      create_function_id: {
        value: rowData.FUNCTION_ID,
        label: rowData.FUNCTION_ID
      },
      old_ai_trainning: rowData
    });
  }

  onCloseDialogUpdateAITrainning = () => {
    this.setState({
      dialogUpdateAITrainning: false,
      create_value: '',
      create_function_id: '',
      old_ai_trainning: null,
      dialogLoading: false,
    });
    // this.getAllAITrainning()
  }

  onCloseDialogCreateAITrainning = () => {
    this.setState({
      dialogCreateAITrainning: false,
      create_value: '',
      create_function_id: '',
    });
    // this.getAllAITrainning()
  }

  createAITrainning = () => {
    this.getNode();
    this.setState({ dialogCreateAITrainning: true });
  }

  onCloseDialogUploadAITrainning = () => {
    this.setState({ dialogUploadAITrainning: false });
  }

  onClickUpload = () => {
    this.setState({ dialogUploadAITrainning: true });
  }

  onDrop = (acceptedFiles, rejectedFiles) => {
    this.setState({
      file: acceptedFiles
    });
  }

  onClickApprove = () => {
    this.setState({ dialogLoading: true })
    this.getApproveAITrainning()
    this.setState({ dialogApprove: true })
  }
  closeClickApprove = () => {
    this.getAllAITrainning();
    this.setState({ dialogApprove: false })
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.dialogApprove}
          //onClose={this.onCloseDialogUploadAITrainning}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Phê duyệt dữ liệu trainning</DialogTitle>
          <DialogContent>
            <MaterialTable
              icons={tableIcons}
              title="Dữ liệu AI cần phê duyệt"
              columns={this.state.columns}
              data={this.state.ai_trainning_approve_array}
              style={{}}
              actions={[
                rowData => ({
                  icon: Check,
                  tooltip: 'Chấp nhận',
                  onClick: (event, rowData) => this.onClickApproveAI(event, rowData),
                }),
                rowData => ({
                  icon: DeleteOutline,
                  tooltip: 'Huỷ bỏ',
                  onClick: (event, rowData) => this.onClickDeleteAITrainning(event, rowData),
                })
              ]}
              options={{
                actionsColumnIndex: -1
              }}
            />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.closeClickApprove}>
              Đóng
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogUploadAITrainning}
          //onClose={this.onCloseDialogUploadAITrainning}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Tải file</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Hãy đảm bảo file bạn nhập vào đúng định dạng mẫu. Tên file là: 'ai_trainning.xlsx'
            </DialogContentText>
            <Dropzone onDrop={this.onDrop}>
              {({ getRootProps, getInputProps, isDragActive }) => {
                return (
                  <div
                    {...getRootProps()}
                    style={{
                      display: 'block',
                      cursor: 'pointer',
                      border: '1px dashed blue',
                      borderRadius: '5px',
                      marginTop: '15px',
                      paddingLeft: '10px',
                      width: '50%',
                      height: '20%'
                    }}
                    className={classNames('dropzone', { 'dropzone--isActive': isDragActive })}
                  >
                    <input {...getInputProps()} />
                    {
                      isDragActive ?
                        <p>Thả file vào đây...</p> :
                        <p>Kéo file vào đây hoặc click để chọn file.</p>
                    }
                  </div>
                )
              }}
            </Dropzone>
            <aside>
              <h2>File đã nhận:</h2>
              <ul>
                {
                  this.state.file.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
                }
              </ul>
            </aside>
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUploadAITrainning}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickUploadAITrainning}>
              Tải lên
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogCreateAITrainning}
          //onClose={this.onCloseDialogCreateAITrainning}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Thêm dữ liệu AI Trainning</DialogTitle>
          <DialogContent>
            {/* <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="function_id"
              label="Mã chức năng - Viết in hoa, không dấu"
              // type="password"
              id="function_id"
              autoComplete="current-function_id"
              value={this.state.create_function_id}
              onChange={this.handleInputFunctionID}
              style={{ marginLeft: '2%', width: '90%' }}
            /> */}
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="value"
              label="Dữ liệu huấn luyện"
              // type="password"
              id="value"
              autoComplete="current-value"
              value={this.state.create_value}
              onChange={this.handleInputCreateValue}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <SelectSearch
                placeholder="Mã chức năng"
                value={this.state.create_function_id}
                onChange={this.handleInputFunctionID}
                options={this.state.node_array}
              />
            </FormControl>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogCreateAITrainning}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickCreateAITrainning}>
              Tạo
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogUpdateAITrainning}
          //onClose={this.onCloseDialogUpdateAITrainning}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa thông tin</DialogTitle>
          <DialogContent>
            {/* <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="function_id"
              label="Mã chức năng - Viết in hoa, không dấu"
              // type="password"
              id="function_id"
              autoComplete="current-function_id"
              value={this.state.create_function_id}
              onChange={this.handleInputFunctionID}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br /> */}
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="value"
              label="Dữ liệu huấn luyện"
              // type="password"
              id="value"
              autoComplete="current-value"
              value={this.state.create_value}
              onChange={this.handleInputCreateValue}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <SelectSearch
                placeholder="Mã chức năng"
                value={this.state.create_function_id}
                onChange={this.handleInputFunctionID}
                options={this.state.node_array}
              />
            </FormControl>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onCloseDialogUpdateAITrainning}>
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateAITrainning}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardActions >
          <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickUpload}>
            Tải lên tệp
          </Button>
          <Button variant="contained" className={classes.buttonFunction}>
            <Link to="/files/ai_trainning.xlsx" target="_blank" download style={{ color: '#ffffff' }}>
              Tải về file mẫu
            </Link>
          </Button>
          <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickApprove}>
            Phê duyệt dữ liệu
          </Button>
        </CardActions>
        <CardContent>
          <MaterialTable
            icons={tableIcons}
            title="Dữ liệu AI"
            columns={this.state.columns}
            data={this.state.ai_trainning_array}
            style={{}}
            actions={[
              {
                icon: AddBox,
                tooltip: 'Thêm',
                isFreeAction: true,
                onClick: this.createAITrainning
              },
              rowData => ({
                icon: Edit,
                tooltip: 'Sửa',
                onClick: (event, rowData) => this.onClickUpdateAITrainning(event, rowData),
              }),
              rowData => ({
                icon: DeleteOutline,
                tooltip: 'Xoá',
                onClick: (event, rowData) => this.onClickDeleteAITrainning(event, rowData),
              })
            ]}
            options={{
              actionsColumnIndex: -1, paging: false
            }}
          />
        </CardContent>
      </Card>
    );
  }

  render() {
    if (this.state.permission.indexOf('AITR') == -1) {
      return <h2>&emsp;&emsp;&emsp;Bạn không có quyền truy cập chức năng này!</h2>;
    } else {
      console.log(this.state.ai_trainning_array);
      const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    Quản lý Dữ liệu AI
                </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Tabs value={this.state.tab_value} textColor="inherit">
              <Tab textColor="inherit" label="Dữ liệu AI" />
            </Tabs>
          </AppBar>
          {this.state.tab_value === 0 && this.renderContent()}
        </React.Fragment>
      );
    }
  }
}

AITrainning.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(AITrainning);