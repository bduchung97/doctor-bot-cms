import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import LoginPage from '../../pages/login_page/LoginPage';
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  }
});

class ChangePassword extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tab_value: 0,
      session_token: '',
      dialogLoading: false,
      old_password: '',
      new_password: '',
      new_password2: '',
      admin_id: ''
    };
    this.handleInputOldPassword = this.handleInputOldPassword.bind(this);
    this.handleInputNewPassword = this.handleInputNewPassword.bind(this);
    this.handleReInputNewPassword = this.handleReInputNewPassword.bind(this);
  }

  componentDidMount() {
    this.setState({
      session_token: sessionStorage.getItem('session_token'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
  }

  onClickCancel = () => {
  }

  onClickChangePassword = () => {
    if (this.state.old_password === '') {
      alert('Mời bạn nhập mật khẩu cũ');
    } else if (this.state.new_password === '') {
      alert('Mời bạn nhập mật khẩu mới');
    } else if (this.state.new_password2 === '') {
      alert('Mời bạn nhập lại mật khẩu mới');
    } else {
      if (this.state.new_password == this.state.new_password2) {
        if (this.state.new_password == this.state.old_password) {
          alert('Mật khẩu mới phải khác mật khẩu cũ!')
        } else {
          this.setState({ dialogLoading: true });
          let data = {
            admin_ID: this.state.admin_id,
            old_password: this.state.old_password,
            new_password: this.state.new_password
          }
          // axios.put(API.change_password_admin, { data: data, session_token: this.state.session_token })
          let hreq = require('../../utils/handle_request/_request');
          axios.put(API.change_password_admin, hreq._request(data, this.state.session_token))
            // let hreq = require('../../utils/handle_request/_request');
            // axios.put(API.change_password, hreq._request(data, this.state.session_token))
            .then(response => {
              this.setState({ dialogLoading: false });

              // var res_data = response.data;
              let hres = require('../../utils/handle_response/handle_response');
              var res_data = hres.handle_response(response.data);
              if (res_data.msg_code == '001') {
                sessionStorage.removeItem('admin_id');
                sessionStorage.removeItem('full_name');
                sessionStorage.removeItem('phone_number');
                sessionStorage.removeItem('email');
                sessionStorage.removeItem('permission');
                sessionStorage.removeItem('username');
                sessionStorage.removeItem('session_token');
                alert('Đổi mật khẩu thành công!');
                ReactDOM.render(<LoginPage />, document.getElementById('root'));
              }
              else if (res_data.msg_code == '000') {
                sessionStorage.removeItem('admin_id');
                sessionStorage.removeItem('full_name');
                sessionStorage.removeItem('phone_number');
                sessionStorage.removeItem('email');
                sessionStorage.removeItem('permission');
                sessionStorage.removeItem('username');
                sessionStorage.removeItem('session_token');
                alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
                ReactDOM.render(<LoginPage />, document.getElementById('root'));
              }
              else {
                alert('Mật khẩu cũ không đúng!')
              }

            })
            .catch(error => {
              console.log(error);
              alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
              this.setState({ dialogLoading: false });
              // sessionStorage.removeItem('admin_id');
              //               sessionStorage.removeItem('full_name');
              //               sessionStorage.removeItem('phone_number');
              //               sessionStorage.removeItem('email');
              //               sessionStorage.removeItem('permission');
              //               sessionStorage.removeItem('username');
              //               sessionStorage.removeItem('session_token');
              //               alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
              //               ReactDOM.render(<LoginPage />, document.getElementById('root'));
            });
        }
      } else {
        alert('Mật khẩu mới không khớp!');
      }
    }
  }

  handleInputOldPassword(event) {
    this.setState({
      old_password: event.target.value.replace(/^\s+/g, '')
    });
  }

  handleInputNewPassword(event) {
    this.setState({
      new_password: event.target.value.replace(/^\s+/g, '')
    });
  }

  handleReInputNewPassword(event) {
    this.setState({
      new_password2: event.target.value.replace(/^\s+/g, '')
    });
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <CardContent>
          <TextField
            //variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Mật khẩu cũ"
            type="password"
            id="oldpassword"
            autoComplete="current-password"
            value={this.state.old_password}
            onChange={this.handleInputOldPassword}
            style={{ marginLeft: '2%', width: '30%' }}
          />
          <br />
          <TextField
            //variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Mật khẩu mới"
            type="password"
            id="newpassword"
            autoComplete="current-password"
            value={this.state.new_password}
            onChange={this.handleInputNewPassword}
            style={{ marginLeft: '2%', width: '30%' }}
          />
          <br />
          <TextField
            //variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Nhập lại mật khẩu mới"
            type="password"
            id="newpassword2"
            autoComplete="current-password"
            value={this.state.new_password2}
            onChange={this.handleReInputNewPassword}
            style={{ marginLeft: '2%', width: '30%' }}
          />
        </CardContent>
        <CardActions >
          <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickChangePassword}>
            Đổi mật khẩu
          </Button>
          <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickCancel}>
            <Link to="/my-account" style={{ color: '#ffffff' }}>
              Huỷ
            </Link>
          </Button>
        </CardActions>
      </Card>
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <AppBar
          component="div"
          className={classes.secondaryBar}
          color="primary"
          position="static"
          elevation={0}
        >
          <Toolbar>
            <Grid container alignItems="center" spacing={1}>
              <Grid item xs>
                <Typography color="inherit" variant="h5" component="h1">
                  Tài khoản
                </Typography>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <AppBar
          component="div"
          className={classes.secondaryBar}
          color="primary"
          position="static"
          elevation={0}
        >
          <Tabs value={this.state.tab_value} textColor="inherit">
            <Tab textColor="inherit" label="Đổi mật khẩu" />
          </Tabs>
        </AppBar>
        {this.state.tab_value === 0 && this.renderContent()}
      </React.Fragment>
    );
  }

}

ChangePassword.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(ChangePassword);