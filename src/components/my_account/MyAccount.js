import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';

import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  }
});

class MyAccount extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      session_token: '',
      dialogLoading: false
    };
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      session_token: sessionStorage.getItem('session_token'),
    })
  }

  onClickLogout = () => {
    this.setState({ dialogLoading: true });
    // axios.post(API.logout_api, { data: '', session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.logout_api, hreq._request('', this.state.session_token))
      .then(response => {
        this.setState({ dialogLoading: false });

        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);

        // var res_data = response.data;
        if (res_data.msg_code == '001') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <CardContent>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Họ và tên:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.admin_info} color="textSecondary" gutterBottom>
                {this.state.fullname}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Tên đăng nhập:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.admin_info} color="textSecondary" gutterBottom>
                {this.state.username}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Số điện thoại:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.admin_info} color="textSecondary" gutterBottom>
                {this.state.phonenumber}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Email:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.admin_info} color="textSecondary" gutterBottom>
                {this.state.email}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions >
          <Button variant="contained" className={classes.buttonFunction}>
            <Link to="/change-password" style={{ color: '#ffffff' }}>
              Đổi mật khẩu
            </Link>
          </Button>
          <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickLogout}>
            Đăng xuất
          </Button>
        </CardActions>
      </Card>
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <AppBar
          component="div"
          className={classes.secondaryBar}
          color="primary"
          position="static"
          elevation={0}
        >
          <Toolbar>
            <Grid container alignItems="center" spacing={1}>
              <Grid item xs>
                <Typography color="inherit" variant="h5" component="h1">
                  Tài khoản
                </Typography>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <AppBar
          component="div"
          className={classes.secondaryBar}
          color="primary"
          position="static"
          elevation={0}
        >
          <Tabs value={this.state.tab_value} textColor="inherit">
            <Tab textColor="inherit" label="Thông tin tài khoản" />
          </Tabs>
        </AppBar>
        {this.state.tab_value === 0 && this.renderContent()}
      </React.Fragment>
    );
  }

}

MyAccount.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(MyAccount);