import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const tableIcons = {
  Add: AddBox,
  Check: Check,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  scheduler_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  },
});

class Scheduler extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      permission: sessionStorage.getItem('permission').split(","),
      admin_id: '',
      tab_value: 0,
      fullname: '',
      username: '',
      phonenumber: '',
      email: '',
      session_token: sessionStorage.getItem('session_token'),
      dialogLoading: true,
      scheduler: {},
      hour: '',
      minute: '',
      second: '',
      day_of_week: '',
      open_dialog_update: false,
    };
    this.handleInputHour = this.handleInputHour.bind(this);
    this.handleInputMinute = this.handleInputMinute.bind(this);
    this.handleInputSecond = this.handleInputSecond.bind(this);
    this.handleInputDayOfWeek = this.handleInputDayOfWeek.bind(this);
  }

  handleInputHour(event) {
    this.setState({
      hour: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputMinute(event) {
    this.setState({
      minute: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputSecond(event) {
    this.setState({
      second: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputDayOfWeek(event) {
    this.setState({
      day_of_week: event.target.value.replace(/^\s+/g, '')
    });
  }

  componentDidMount() {
    this.setState({
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
    })
    this.getAllScheduler();
  }

  getAllScheduler() {
    // axios.post(API.show_all_handbook, { data: '', session_token: this.state.session_token })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_scheduler, hreq._request('', this.state.session_token))
      .then(response => {
        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var data = res_data.train_AI_scheduler;
          if (data.IN_PROCESS == true) {
            data.IN_PROCESS = 'Đang thực hiện huấn luyện...'
          } else {
            data.IN_PROCESS = 'Đang chờ thực thiện huấn luyện'
          }
          data.DAY_OF_WEEK = parseInt(data.DAY_OF_WEEK) + 1
          console.log(data)
          this.setState({
            scheduler: data,
            hour: data.HOUR,
            minute: data.MINUTE,
            second: data.SECOND,
            day_of_week: data.DAY_OF_WEEK,
            dialogLoading: false
          });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });
  }

  updateScheduler = () => {
    this.setState({ dialogLoading: true });
    let data = {
      updated_data: {
        HOUR: this.state.hour,
        MINUTE: this.state.minute,
        SECOND: this.state.second,
        DAY_OF_WEEK: this.state.day_of_week,
        UPDATED_BY: this.state.admin_id,
        UPDATED_AT: new Date()
      }
    }
    data.updated_data.DAY_OF_WEEK = parseInt(data.updated_data.DAY_OF_WEEK) - 1
    console.log(data);
    let hreq = require('../../utils/handle_request/_request');
    axios.put(API.update_scheduler, hreq._request(data, this.state.session_token))
      // axios.put(API.update_handbook, { data: data, session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            hour: '',
            minute: '',
            second: '',
            day_of_week: '',
            open_dialog_update: false,
          });
          this.getAllScheduler();
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({
            dialogUpdateHandbook: false,
          })
        }

      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // if (error.response.status == 400) {
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
        // if (error.response.status == 500) {
        //   alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        //   sessionStorage.removeItem('admin_id');
        //   sessionStorage.removeItem('full_name');
        //   sessionStorage.removeItem('phone_number');
        //   sessionStorage.removeItem('email');
        //   sessionStorage.removeItem('permission');
        //   sessionStorage.removeItem('username');
        //   sessionStorage.removeItem('session_token');
        //   alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        //   ReactDOM.render(<LoginPage />, document.getElementById('root'));
        // }
      });

  }

  openDialog = () => {
    this.setState({ open_dialog_update: true });
  }
  closeDialog = () => {
    this.setState({
      open_dialog_update: false, hour: '',
      minute: '',
      second: '',
      day_of_week: '',
    });
    // this.getAllScheduler()
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.open_dialog_update}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa lập lịch</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="hour"
              label="Giờ 0-23"
              // type="password"
              id="hour"
              autoComplete="current-hour"
              value={this.state.hour}
              onChange={this.handleInputHour}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="minute"
              label="Phút 0-59"
              // type="password"
              id="minute"
              autoComplete="current-minute"
              value={this.state.minute}
              onChange={this.handleInputMinute}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="second"
              label="Giây 0-59"
              // type="password"
              id="second"
              autoComplete="current-second"
              value={this.state.second}
              onChange={this.handleInputSecond}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="day_of_week"
              label="Ngày trong tuần 2-8"
              // type="password"
              id="day_of_week"
              autoComplete="current-day_of_week"
              value={this.state.day_of_week}
              onChange={this.handleInputDayOfWeek}
              style={{ marginLeft: '2%', width: '90%' }}
            />
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.closeDialog} >
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateScheduler}>
              Cập nhật
            </Button>
          </DialogActions>
        </Dialog>
        <CardContent>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Trạng thái:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.scheduler_info} color="textSecondary" gutterBottom>
                {this.state.scheduler.IN_PROCESS}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Giờ:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.scheduler_info} color="textSecondary" gutterBottom>
                {this.state.scheduler.HOUR}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Phút:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.scheduler_info} color="textSecondary" gutterBottom>
                {this.state.scheduler.MINUTE}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Giây:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.scheduler_info} color="textSecondary" gutterBottom>
                {this.state.scheduler.SECOND}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2} xs={12}>
            <Grid item style={{ width: '15%' }}>
              <Typography color="textSecondary" gutterBottom>
                Ngày trong tuần:
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.scheduler_info} color="textSecondary" gutterBottom>
                Thứ {this.state.scheduler.DAY_OF_WEEK}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions >
          <Button variant="contained" className={classes.buttonFunction} onClick={this.openDialog}>
            Thay đổi lịch
          </Button>
        </CardActions>
      </Card>
    );
  }

  render() {
    if (this.state.permission.indexOf('SCHE') == -1) {
      return <h2>&emsp;&emsp;&emsp;Bạn không có quyền truy cập chức năng này!</h2>;
    } else {
      console.log(this.state.handbook_array);
      const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    Lập lịch
                </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Tabs value={this.state.tab_value} textColor="inherit">
              <Tab textColor="inherit" label="Lập lịch" />
            </Tabs>
          </AppBar>
          {this.state.tab_value === 0 && this.renderContent()}
        </React.Fragment>
      );
    }
  }
}

Scheduler.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Scheduler);