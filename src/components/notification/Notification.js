import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import API from '../../api/api_config';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import TextField from '@material-ui/core/TextField';
import MaterialTable from 'material-table';
import LoginPage from '../../pages/login_page/LoginPage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ToggleOn from '@material-ui/icons/ToggleOn';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import Group from '@material-ui/icons/Group';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MuiTree from 'material-ui-tree';
import CardMedia from '@material-ui/core/CardMedia';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SelectSearch from 'react-select';

import Generic_img from '../../assets/generic_sq.png';
import Text_img from '../../assets/text_sq.png';
import List_img from '../../assets/list_sq.png';
import Media_img from '../../assets/media_sq.png';

const tableIcons = {
  Add: AddBox,
  ToggleOn: ToggleOn,
  Clear: Clear,
  Delete: DeleteOutline,
  DetailPanel: ChevronRight,
  Edit: Edit,
  Export: SaveAlt,
  Filter: FilterList,
  FirstPage: FirstPage,
  LastPage: LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  ResetSearch: Clear,
  Search: Search,
  SortArrow: ArrowUpward,
  ThirdStateCheck: Remove,
  ViewColumn: ViewColumn,
  Group: Group
};
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  dropzone: {
    display: 'block',
    cursor: 'pointer',
    border: '1px dashed blue',
    borderRadius: '5px',
    marginTop: '15px',
    paddingLeft: '10px'
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  popUp: {
    marginLeft: '90%'
  },
  content: {
    width: '98%',
    alignItems: 'center',
    marginLeft: '1%',
    marginTop: '1%'
  },
  admin_info: {
    fontWeight: 'bold'
  },
  buttonFunction: {
    marginLeft: '2%',
    background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    // height: 48,
    // padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '17%'
  },
  media_: {
    height: 260,
    marginBottom: '2%'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Notification extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fullname: sessionStorage.getItem('full_name'),
      username: sessionStorage.getItem('username'),
      phonenumber: sessionStorage.getItem('phone_number'),
      email: sessionStorage.getItem('email'),
      admin_id: sessionStorage.getItem('admin_id'),
      session_token: sessionStorage.getItem('session_token'),
      permission: sessionStorage.getItem('permission'),
      dialogLoading: true,
      noti_data: [],
      noti_raw: [], columns: [
        { title: 'Tên', field: 'TITLE' },
        { title: 'Trạng thái', field: 'STATUS' },
      ],
      dialogCreateNoti: false,
      dialogShowUpdate: false,
      dialogAddTemplate: false,
      dialogUpdateText: false,
      dialogUpdateMedia: false,
      dialogUpdateList: false,
      dialogUpdateGeneric: false,
      showActionAddTemplate: true,
      showActionUpdateTemplate: true,
      showSelectionType: true,
      showAddTextTemplate: false,
      showAddMediaTemplate: false,
      showAddListTemplate: false,
      showActionAddListTemplate: false,
      showAddGenericTemplate: false,
      showAddGenericElement: false,
      showActionAddGenericTemplate: false,
      showActionAddGenericElement: false,
      showActionUpdateGenericElement: false,
      showActionAddMedia: false,
      showAddTarget: false,
      showActionTarget: false,
      showDialogUpdateNoti: false,
      index: null,
      index_leaf: null,
      create_text: '',
      create_media_type: '',
      create_media_url: '',
      create_list_big_title: '',
      create_list_big_subtitle: '',
      create_list_big_image_url: '',
      create_list_big_url: '',
      create_list_content_1: '',
      create_list_subcontent_1: '',
      create_list_content_2: '',
      create_list_subcontent_2: '',
      create_list_content_3: '',
      create_list_subcontent_3: '',
      create_list_button_title: '',
      create_list_button_type: true,
      create_list_button_url: '',
      create_list_button_funtion_id: '',
      create_generic: [],
      create_generic_title: '',
      create_generic_subtitle: '',
      create_generic_image_url: '',
      create_generic_button_title_1: '',
      create_generic_button_type_1: false,
      create_generic_button_url_1: '',
      create_generic_button_funtion_id_1: '',
      create_generic_button_title_2: '',
      create_generic_button_type_2: false,
      create_generic_button_url_2: '',
      create_generic_button_funtion_id_2: '',
      create_generic_button_title_3: '',
      create_generic_button_type_3: false,
      create_generic_button_url_3: '',
      create_generic_button_funtion_id_3: '',
      create_noti_name: '',
      create_hour: '0',
      create_minute: '00',
      create_second: '00',
      create_schedule_type: true,
      create_day_of_week: '',
      create_day_of_month: '',
      create_month: '',
      create_status: true,
      showTempaltes: false,
      showActionSelectType: true,
      showActionAddText: false,
      create_value: null,
      create_target: false,
      showScheduleNoti: false,
      showActionScheduleNoti: false,
      tab_value: 0,
      node_array: [],
      leaf_data: [{
        TYPE: ''
      }],
      group_data: [],
      target_array: [],
      create_template: [],
    };
    this.handleInputStatus = this.handleInputStatus.bind(this);
    this.handleInputHour = this.handleInputHour.bind(this);
    this.handleInputMinute = this.handleInputMinute.bind(this);
    this.handleInputSecond = this.handleInputSecond.bind(this);
    this.handleInputDayOfWeek = this.handleInputDayOfWeek.bind(this);
    this.handleInputDayOfMonth = this.handleInputDayOfMonth.bind(this);
    this.handleInputScheduleType = this.handleInputScheduleType.bind(this);
    this.handleInputMonth = this.handleInputMonth.bind(this);
    this.handleInputTarget = this.handleInputTarget.bind(this);
    this.handleInputNotiName = this.handleInputNotiName.bind(this);
    this.handleInputText = this.handleInputText.bind(this);
    this.handleInputMediaType = this.handleInputMediaType.bind(this);
    this.handleInputMediaURL = this.handleInputMediaURL.bind(this);
    this.handleInputListBigTitle = this.handleInputListBigTitle.bind(this);
    this.handleInputListBigSubtitle = this.handleInputListBigSubtitle.bind(this);
    this.handleInputListBigImageUrl = this.handleInputListBigImageUrl.bind(this);
    this.handleInputListBigUrl = this.handleInputListBigUrl.bind(this);
    this.handleInputListContent1 = this.handleInputListContent1.bind(this);
    this.handleInputListSubContent1 = this.handleInputListSubContent1.bind(this);
    this.handleInputListContent2 = this.handleInputListContent2.bind(this);
    this.handleInputListSubContent2 = this.handleInputListSubContent2.bind(this);
    this.handleInputListContent3 = this.handleInputListContent3.bind(this);
    this.handleInputListSubContent3 = this.handleInputListSubContent3.bind(this);
    this.handleInputListButtonTitle = this.handleInputListButtonTitle.bind(this);
    this.handleInputListButtonType = this.handleInputListButtonType.bind(this);
    this.handleInputListButtonUrl = this.handleInputListButtonUrl.bind(this);
    this.handleInputListButtonFunctionID = this.handleInputListButtonFunctionID.bind(this);
    this.handleInputGenericTitle = this.handleInputGenericTitle.bind(this);
    this.handleInputGenericSubtitle = this.handleInputGenericSubtitle.bind(this);
    this.handleInputGenericImageUrl = this.handleInputGenericImageUrl.bind(this);
    this.handleInputGenericButtonTitle1 = this.handleInputGenericButtonTitle1.bind(this);
    this.handleInputGenericButtonType1 = this.handleInputGenericButtonType1.bind(this);
    this.handleInputGenericButtonUrl1 = this.handleInputGenericButtonUrl1.bind(this);
    this.handleInputGenericButtonFunctionID1 = this.handleInputGenericButtonFunctionID1.bind(this);
    this.handleInputGenericButtonTitle2 = this.handleInputGenericButtonTitle2.bind(this);
    this.handleInputGenericButtonType2 = this.handleInputGenericButtonType2.bind(this);
    this.handleInputGenericButtonUrl2 = this.handleInputGenericButtonUrl2.bind(this);
    this.handleInputGenericButtonFunctionID2 = this.handleInputGenericButtonFunctionID2.bind(this);
    this.handleInputGenericButtonTitle3 = this.handleInputGenericButtonTitle3.bind(this);
    this.handleInputGenericButtonType3 = this.handleInputGenericButtonType3.bind(this);
    this.handleInputGenericButtonUrl3 = this.handleInputGenericButtonUrl3.bind(this);
    this.handleInputGenericButtonFunctionID3 = this.handleInputGenericButtonFunctionID3.bind(this);
  }

  handleInputStatus(event) {
    this.setState({
      create_status: event.target.value
    });
  }
  handleInputHour(event) {
    this.setState({
      create_hour: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputMinute(event) {
    this.setState({
      create_minute: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputSecond(event) {
    this.setState({
      create_second: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputDayOfWeek(event) {
    this.setState({

      create_day_of_week: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputDayOfMonth(event) {
    this.setState({
      create_day_of_month: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputScheduleType(event) {
    this.setState({
      create_schedule_type: event.target.value
    });
  }
  handleInputMonth(event) {
    this.setState({
      create_month: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputTarget(event) {
    this.setState({
      create_target: event.target.value
    });
  }
  handleInputNotiName(event) {
    this.setState({
      create_noti_name: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericTitle(event) {
    this.setState({
      create_generic_title: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericSubtitle(event) {
    this.setState({
      create_generic_subtitle: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericImageUrl(event) {
    this.setState({
      create_generic_image_url: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericButtonTitle1(event) {
    this.setState({
      create_generic_button_title_1: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericButtonType1(event) {
    this.setState({
      create_generic_button_type_1: event.target.value
    });
  }
  handleInputGenericButtonUrl1(event) {
    this.setState({
      create_generic_button_url_1: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericButtonFunctionID1(event) {
    this.setState({
      create_generic_button_funtion_id_1: event
    });
  }
  handleInputGenericButtonTitle2(event) {
    this.setState({
      create_generic_button_title_2: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericButtonType2(event) {
    this.setState({
      create_generic_button_type_2: event.target.value
    });
  }
  handleInputGenericButtonUrl2(event) {
    this.setState({
      create_generic_button_url_2: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericButtonFunctionID2(event) {
    this.setState({
      create_generic_button_funtion_id_2: event
    });
  }
  handleInputGenericButtonTitle3(event) {
    this.setState({
      create_generic_button_title_3: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericButtonType3(event) {
    this.setState({
      create_generic_button_type_3: event.target.value
    });
  }
  handleInputGenericButtonUrl3(event) {
    this.setState({
      create_generic_button_url_3: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputGenericButtonFunctionID3(event) {
    this.setState({
      create_generic_button_funtion_id_3: event
    });
  }
  handleInputListBigTitle(event) {
    this.setState({
      create_list_big_title: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListBigSubtitle(event) {
    this.setState({
      create_list_big_subtitle: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListBigImageUrl(event) {
    this.setState({
      create_list_big_image_url: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListBigUrl(event) {
    this.setState({
      create_list_big_url: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListContent1(event) {
    this.setState({
      create_list_content_1: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListSubContent1(event) {
    this.setState({
      create_list_subcontent_1: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListContent2(event) {
    this.setState({
      create_list_content_2: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListSubContent2(event) {
    this.setState({
      create_list_subcontent_2: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListContent3(event) {
    this.setState({
      create_list_content_3: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListSubContent3(event) {
    this.setState({
      create_list_subcontent_3: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListButtonUrl(event) {
    this.setState({
      create_list_button_url: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputListButtonType(event) {
    this.setState({
      create_list_button_type: event.target.value
    });
  }
  handleInputListButtonFunctionID(event) {
    this.setState({
      create_list_button_funtion_id: event
    });
  }
  handleInputListButtonTitle(event) {
    this.setState({
      create_list_button_title: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputMediaURL(event) {
    this.setState({
      create_media_url: event.target.value.replace(/^\s+/g, '')
    });
  }
  handleInputMediaType(event) {
    this.setState({
      create_media_type: event.target.value
    });
  }
  handleInputText(event) {
    let str = event.target.value.replace(/^\s+/g, '');
    if (str.length > 2000) {
      alert('Đã đạt giới hạn về ký tự')
    } else {
      this.setState({
        create_text: event.target.value.replace(/^\s+/g, '')
      });
    }
  }
  componentDidMount() {
    this.getNotification();
  }

  getNotification() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_noti_all, hreq._request('', this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          console.log(res_data.notification_data);
          this.setState({
            noti_raw: res_data.notification_data,
            dialogLoading: false,
            dialogCreateNoti: false,
            dialogShowUpdate: false,
            dialogAddTemplate: false,
            dialogUpdateText: false,
            dialogUpdateMedia: false,
            dialogUpdateList: false,
            dialogUpdateGeneric: false,
            showActionAddTemplate: true,
            showActionUpdateTemplate: true,
            showSelectionType: true,
            showAddTextTemplate: false,
            showAddMediaTemplate: false,
            showAddListTemplate: false,
            showAddGenericTemplate: false,
            showAddGenericElement: false,
            showActionAddGenericElement: false,
            showActionUpdateGenericElement: false,
            showActionAddMedia: false,
            showAddTarget: false,
            showActionTarget: false,
            index: null,
            index_leaf: null,
            create_text: '',
            create_media_type: '',
            create_media_url: '',
            create_list_big_title: '',
            create_list_big_subtitle: '',
            create_list_big_image_url: '',
            create_list_big_url: '',
            create_list_content_1: '',
            create_list_subcontent_1: '',
            create_list_content_2: '',
            create_list_subcontent_2: '',
            create_list_content_3: '',
            create_list_subcontent_3: '',
            create_list_button_title: '',
            create_list_button_type: true,
            create_list_button_url: '',
            create_list_button_funtion_id: '',
            create_generic: [],
            create_generic_title: '',
            create_generic_subtitle: '',
            create_generic_image_url: '',
            create_generic_button_title_1: '',
            create_generic_button_type_1: false,
            create_generic_button_url_1: '',
            create_generic_button_funtion_id_1: '',
            create_generic_button_title_2: '',
            create_generic_button_type_2: false,
            create_generic_button_url_2: '',
            create_generic_button_funtion_id_2: '',
            create_generic_button_title_3: '',
            create_generic_button_type_3: false,
            create_generic_button_url_3: '',
            create_generic_button_funtion_id_3: '',
            create_noti_name: '',
            create_hour: '0',
            create_minute: '00',
            create_second: '00',
            create_schedule_type: true,
            create_day_of_week: '',
            create_day_of_month: '',
            create_month: '',
            create_status: true,
            showTempaltes: false,
            showActionSelectType: true,
            showActionAddText: false,
            create_value: null,
            create_target: false,
            showScheduleNoti: false,
            showActionScheduleNoti: false,
            tab_value: 0,
            node_array: [],
            leaf_data: [{
              TYPE: ''
            }],
            group_data: [],
            target_array: [],
            create_template: [],
            showDialogUpdateNoti: false,
          });
          var array = res_data.notification_data;

          for (let i = 0; i < array.length; i++) {
            if (array[i].STATUS == true) {
              array[i].STATUS = 'Đang bật'
            }
            if (array[i].STATUS == false) {
              array[i].STATUS = 'Đang tắt'
            }
          }
          this.setState({ noti_data: array })
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  getGroup() {
    this.setState({ dialogLoading: true, })
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_group_noti, hreq._request('', this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            group_data: res_data.group_data,
            dialogLoading: false,
          });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  getNode() {
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.show_node_all_, hreq._request('', this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          var a = [];
          res_data.node_data.map((e, i) => {
            a.push({
              value: e.FUNCTION_ID,
              label: e.FUNCTION_ID + '- Mô tả: ' + e.DESCRIPTION
            })
          })
          this.setState({
            node_array: a,
            dialogLoading: false
          });
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  pushNotiImmediately = () => {
    if (this.state.showDialogUpdateNoti == true) {
      if (this.state.create_noti_name == 'Thời tiết') {
        alert('Thông báo thời tiết không được đẩy tức thời, cần đặt lịch!');
      } else {
        this.setState({ dialogLoading: true });
        let data = {
          notification_ID: this.state.index
        }
        let hreq = require('../../utils/handle_request/_request');
        axios.post(API.push_noti_now, hreq._request(data, this.state.session_token))
          // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
          .then(response => {

            // var res_data = response.data;
            let hres = require('../../utils/handle_response/handle_response');
            var res_data = hres.handle_response(response.data);
            if (res_data.msg_code == '001') {
              alert('Đẩy thông báo thành công!')
              this.setState({
                dialogCreateNoti: false
              });
              this.getNotification()
            }
            else if (res_data.msg_code == '000') {
              sessionStorage.removeItem('admin_id');
              sessionStorage.removeItem('full_name');
              sessionStorage.removeItem('phone_number');
              sessionStorage.removeItem('email');
              sessionStorage.removeItem('permission');
              sessionStorage.removeItem('username');
              sessionStorage.removeItem('session_token');
              alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
              ReactDOM.render(<LoginPage />, document.getElementById('root'));
            }
            else {
              alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
              this.setState({ dialogLoading: false });
            }
          })
          .catch(error => {
            console.log(error);
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
            // sessionStorage.removeItem('admin_id');
            // sessionStorage.removeItem('full_name');
            // sessionStorage.removeItem('phone_number');
            // sessionStorage.removeItem('email');
            // sessionStorage.removeItem('permission');
            // sessionStorage.removeItem('username');
            // sessionStorage.removeItem('session_token');
            // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            // ReactDOM.render(<LoginPage />, document.getElementById('root'));
          });
      }
    } else {
      if (this.state.create_noti_name == '') {
        alert('Bạn chưa điền tên thông báo!');
      }
      else if (this.state.create_noti_name == 'Thời tiết') {
        alert('Không được đặt thông báo trùng tên!');
      }
      else {
        this.setState({ dialogLoading: true });
        let data = {
          title: this.state.create_noti_name,
          template: this.state.create_template,
          created_by: this.state.admin_id,
          target: this.state.target_array,
        }
        if (this.state.create_target == false) {
          data.target = ['*']
        }
        let hreq = require('../../utils/handle_request/_request');
        axios.post(API.push_noti_immediate, hreq._request(data, this.state.session_token))
          // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
          .then(response => {

            // var res_data = response.data;
            let hres = require('../../utils/handle_response/handle_response');
            var res_data = hres.handle_response(response.data);
            if (res_data.msg_code == '001') {
              this.setState({
                showDialogUpdateNoti: false
              });
              this.getNotification()
            }
            else if (res_data.msg_code == '000') {
              sessionStorage.removeItem('admin_id');
              sessionStorage.removeItem('full_name');
              sessionStorage.removeItem('phone_number');
              sessionStorage.removeItem('email');
              sessionStorage.removeItem('permission');
              sessionStorage.removeItem('username');
              sessionStorage.removeItem('session_token');
              alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
              ReactDOM.render(<LoginPage />, document.getElementById('root'));
            }
            else {
              alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
              this.setState({ dialogLoading: false });
            }
          })
          .catch(error => {
            console.log(error);
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
            // sessionStorage.removeItem('admin_id');
            // sessionStorage.removeItem('full_name');
            // sessionStorage.removeItem('phone_number');
            // sessionStorage.removeItem('email');
            // sessionStorage.removeItem('permission');
            // sessionStorage.removeItem('username');
            // sessionStorage.removeItem('session_token');
            // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            // ReactDOM.render(<LoginPage />, document.getElementById('root'));
          });
      }
    }
  }

  pushScheduleNoti = () => {
    if (this.state.create_noti_name == '') {
      alert('Bạn chưa điền tên thông báo!');
    }
    else if (this.state.create_noti_name == 'Thời tiết') {
      alert('Không được đặt thông báo trùng tên!');
    }
    else {
      this.setState({ dialogLoading: true });
      var month = '*';
      var day_of_week = '*';
      var day_of_month = '*';
      if (this.state.create_schedule_type == true) {
        if (this.state.create_day_of_week !== '*') {
          if (this.state.create_day_of_week == 8) {
            this.setState({ create_day_of_week: 0 })
          } else {
            day_of_week = parseInt(this.state.create_day_of_week) - 1
          }
        }
      } else {
        if (this.state.create_day_of_month !== '*') {
          day_of_month = this.state.create_day_of_month;
        }
        if (this.state.create_month !== '*') {
          month = parseInt(this.state.create_month) - 1
        }
      }
      let data = {
        title: this.state.create_noti_name,
        templates: this.state.create_template,
        created_by: this.state.admin_id,
        target: this.state.target_array,
        status: this.state.create_status,
        schedule_second: this.state.create_second,
        schedule_minute: this.state.create_minute,
        schedule_hour: this.state.create_hour,
        schedule_day_of_month: day_of_month,
        schedule_month: month.toString(),
        schedule_day_of_week: day_of_week.toString(),
      }
      if (this.state.create_target == false) {
        data.target = ['*']
      }
      console.log(data);
      let hreq = require('../../utils/handle_request/_request');
      axios.post(API.push_noti, hreq._request(data, this.state.session_token))
        // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
        .then(response => {

          // var res_data = response.data;
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          if (res_data.msg_code == '001') {
            this.setState({
              dialogCreateNoti: false
            });
            this.getNotification()
          }
          else if (res_data.msg_code == '000') {
            sessionStorage.removeItem('admin_id');
            sessionStorage.removeItem('full_name');
            sessionStorage.removeItem('phone_number');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('permission');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('session_token');
            alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
            ReactDOM.render(<LoginPage />, document.getElementById('root'));
          }
          else {
            alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
            this.setState({ dialogLoading: false });
          }
        })
        .catch(error => {
          console.log(error);
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
          // sessionStorage.removeItem('admin_id');
          // sessionStorage.removeItem('full_name');
          // sessionStorage.removeItem('phone_number');
          // sessionStorage.removeItem('email');
          // sessionStorage.removeItem('permission');
          // sessionStorage.removeItem('username');
          // sessionStorage.removeItem('session_token');
          // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          // ReactDOM.render(<LoginPage />, document.getElementById('root'));
        });
    }
  }

  onClickDeleteNoti = (event, rowData) => {
    this.setState({ dialogLoading: true });
    if (rowData.STATUS == 'Đang bật') {
      rowData.STATUS = true
    } else {
      rowData.STATUS = false
    }
    let data = {
      notification_ID: rowData._id,
      notification_status: rowData.STATUS,
      notification_type: rowData.TYPE,
    }
    console.log(data);
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.delete_noti, hreq._request(data, this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            dialogCreateNoti: false
          });
          this.getNotification()
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  updateNoti = () => {
    this.setState({ dialogLoading: true });
    var month = '*';
    var day_of_week = '*';
    var day_of_month = '*';
    if (this.state.create_schedule_type == true) {
      if (this.state.create_day_of_week !== '*') {
        if (this.state.create_day_of_week == 8) {
          this.setState({ create_day_of_week: 0 })
        } else {
          day_of_week = parseInt(this.state.create_day_of_week) - 1
        }
      }
    } else {
      if (this.state.create_day_of_month !== '*') {
        day_of_month = this.state.create_day_of_month;
      }
      if (this.state.create_month !== '*') {
        month = parseInt(this.state.create_month) - 1
      }
    }
    let data = {
      notification_ID: this.state.index,
      updated_data: {
        TITLE: this.state.create_noti_name,
        STATUS: this.state.create_status,
        SCHEDULE_SECOND: this.state.create_second,
        SCHEDULE_MINUTE: this.state.create_minute,
        SCHEDULE_HOUR: this.state.create_hour,
        SCHEDULE_DAY_OF_MONTH: day_of_month,
        SCHEDULE_MONTH: month.toString(),
        SCHEDULE_DAY_OF_WEEK: day_of_week.toString(),
        UPDATED_BY: this.state.admin_id,
        UPDATED_AT: new Date()
      }
    }
    console.log(data);
    let hreq = require('../../utils/handle_request/_request');
    axios.post(API.update_noti, hreq._request(data, this.state.session_token))
      // axios.post(API.show_all_user, { data: '', session_token: this.state.session_token })
      .then(response => {

        // var res_data = response.data;
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        if (res_data.msg_code == '001') {
          this.setState({
            showDialogUpdateNoti: false
          });
          this.getNotification()
        }
        else if (res_data.msg_code == '000') {
          sessionStorage.removeItem('admin_id');
          sessionStorage.removeItem('full_name');
          sessionStorage.removeItem('phone_number');
          sessionStorage.removeItem('email');
          sessionStorage.removeItem('permission');
          sessionStorage.removeItem('username');
          sessionStorage.removeItem('session_token');
          alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
          ReactDOM.render(<LoginPage />, document.getElementById('root'));
        }
        else {
          alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
          this.setState({ dialogLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        alert('Đã có lỗi xảy ra. Xin vui lòng thử lại!');
        this.setState({ dialogLoading: false });
        // sessionStorage.removeItem('admin_id');
        // sessionStorage.removeItem('full_name');
        // sessionStorage.removeItem('phone_number');
        // sessionStorage.removeItem('email');
        // sessionStorage.removeItem('permission');
        // sessionStorage.removeItem('username');
        // sessionStorage.removeItem('session_token');
        // alert('Phiên làm việc hết hạn. Xin vui lòng đăng nhập lại!');
        // ReactDOM.render(<LoginPage />, document.getElementById('root'));
      });
  }

  onClickUpdateNoti = (event, rowData) => {
    console.log(rowData)
    this.setState({
      showDialogUpdateNoti: true,
      create_noti_name: rowData.TITLE,
      create_hour: rowData.SCHEDULE_HOUR,
      create_minute: rowData.SCHEDULE_MINUTE,
      create_second: rowData.SCHEDULE_SECOND,
      index: rowData._id
    })
    if (rowData.SCHEDULE_DAY_OF_WEEK == '*' && rowData.SCHEDULE_DAY_OF_MONTH == '*') {
      this.setState({
        create_day_of_month: rowData.SCHEDULE_DAY_OF_MONTH,
        create_schedule_type: true,
        create_day_of_week: rowData.SCHEDULE_DAY_OF_WEEK,
        create_month: rowData.SCHEDULE_MONTH
      })

    } else if (rowData.SCHEDULE_DAY_OF_WEEK !== '*') {
      this.setState({
        create_day_of_month: rowData.SCHEDULE_DAY_OF_MONTH,
        create_schedule_type: true,
        create_day_of_week: (parseInt(rowData.SCHEDULE_DAY_OF_WEEK) + 1).toString(),
        create_month: rowData.SCHEDULE_MONTH,
      })
    }
    else if (rowData.SCHEDULE_DAY_OF_MONTH !== '*') {
      this.setState({
        create_day_of_month: rowData.SCHEDULE_DAY_OF_MONTH,
        create_schedule_type: false,
        create_month: rowData.SCHEDULE_MONTH
      })
    }
    else if (rowData.SCHEDULE_MONTH !== '*') {
      this.setState({
        create_day_of_month: rowData.SCHEDULE_DAY_OF_MONTH,
        create_schedule_type: false,
        create_month: rowData.SCHEDULE_MONTH
      })
    }
    if (rowData.STATUS == 'Đang bật') {
      this.setState({
        create_status: true,
      })
    }
    if (rowData.STATUS == 'Đang tắt') {
      this.setState({
        create_status: false,
      })
    }
    if (rowData.SCHEDULE_MONTH !== '*') {
      this.setState({
        create_month: (parseInt(rowData.SCHEDULE_MONTH) + 1).toString()
      })
    }
  }
  onClickCloseUpdateNoti = () => {
    this.setState({
      showDialogUpdateNoti: false,
      create_noti_name: '',
      create_hour: '0',
      create_minute: '00',
      create_second: '00',
      create_schedule_type: true,
      create_day_of_week: '*',
      create_day_of_month: '*',
      create_month: '*',
      create_status: true,
    })
  }

  scheduleNoti = () => {
    this.setState({
      showScheduleNoti: true,
      showActionScheduleNoti: true,
      showAddTarget: false,
      showActionTarget: false,
    })
  }

  closeScheduleNoti = () => {
    this.setState({
      showScheduleNoti: false,
      showActionScheduleNoti: false,
      showAddTarget: true,
      showActionTarget: true,
    })
  }

  closeTarget = () => {
    this.setState({
      showAddTarget: false,
      showActionTarget: false,
      create_template: [],
    })
    if (this.state.create_template[0].TYPE == 'TEXT') {
      this.setState({
        showActionAddText: true,
        showAddTextTemplate: true
      })
    }
    if (this.state.create_template[0].TYPE == 'MEDIA') {
      this.setState({
        showActionAddMedia: true,
        showAddMediaTemplate: true
      })
    }
    if (this.state.create_template[0].TYPE == 'LIST') {
      this.setState({
        showActionAddListTemplate: true,
        showAddListTemplate: true
      })
    }
    if (this.state.create_template[0].TYPE == 'GENERIC') {
      this.setState({
        showActionAddGenericTemplate: true,
        showAddGenericTemplate: true
      })
    }
  }

  handleAddTemplate = () => {
    this.getGroup();
    if (this.state.showAddTextTemplate == true) {
      let value = {
        text: this.state.create_text
      }
      var array = this.state.create_template;
      array.push({ TYPE: 'TEXT', VALUE: value })
      this.setState({
        create_template: array,
        showAddTarget: true,
        showActionTarget: true,
        showActionAddText: false,
        showAddTextTemplate: false
      })
    }
    if (this.state.showAddMediaTemplate == true) {
      let value = {
        attachment: {
          type: "template",
          payload: {
            template_type: "media",
            elements: [{
              media_type: this.state.create_media_type,
              url: this.state.create_media_url
            }]
          }
        }
      }
      var array = this.state.create_template;
      array.push({ TYPE: 'MEDIA', VALUE: value })
      this.setState({
        create_template: array,
        showAddTarget: true,
        showActionTarget: true,
        showActionAddMedia: false,
        showAddMediaTemplate: false
      })
    }
    if (this.state.showAddListTemplate == true) {
      let value = {
        attachment: {
          type: "template",
          payload: {
            template_type: "list",
            top_element_style: "LARGE",
            elements: [
              {
                title: this.state.create_list_big_title,
                subtitle: this.state.create_list_big_subtitle,
                image_url: this.state.create_list_big_image_url,
                default_action: {
                  type: "web_url",
                  url: this.state.create_list_big_url,
                }
              },
              {
                title: this.state.create_list_content_1,
                subtitle: this.state.create_list_subcontent_1,
              },
            ],
          }
        }
      }
      if (this.state.create_list_button_type == true) {
        value.attachment.payload = Object.assign(
          value.attachment.payload,
          {
            buttons: [{
              title: this.state.create_list_button_title,
              type: "web_url",
              url: this.state.create_list_button_url,
              webview_height_ratio: "tall",
            }]
          }
        )
      }
      if (this.state.create_list_button_type == false) {
        value.attachment.payload = Object.assign(
          value.attachment.payload,
          {
            buttons: [{
              title: this.state.create_list_button_title,
              type: "postback",
              payload: this.state.create_list_button_funtion_id.value,
            }]
          }
        )
      }
      if (this.state.create_list_content_2 !== '') {
        value.attachment.payload.elements.push({
          title: this.state.create_list_content_2,
          subtitle: this.state.create_list_subcontent_2,
        })
      }
      if (this.state.create_list_content_3 !== '') {
        value.attachment.payload.elements.push({
          title: this.state.create_list_content_3,
          subtitle: this.state.create_list_subcontent_3,
        })
      }
      var array = this.state.create_template;
      array.push({ TYPE: 'LIST', VALUE: value })
      this.setState({
        create_template: array,
        showAddTarget: true,
        showActionTarget: true,
        showActionAddListTemplate: false,
        showAddListTemplate: false
      })
    }
    if (this.state.showAddGenericTemplate == true) {
      var a = this.state.create_generic
      for (let i = 0; i < a.length; i++) {
        delete a[i].tableData
      }
      let value = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: a
          }
        }
      }
      var array = this.state.create_template;
      array.push({ TYPE: 'GENERIC', VALUE: value })
      this.setState({
        create_template: array,
        showAddTarget: true,
        showActionTarget: true,
        showActionAddGenericTemplate: false,
        showAddGenericTemplate: false
      })
    }
  }

  handleAddGenericElement = () => {
    var i = this.state.create_generic.length;
    if (i < 10) {
      this.state.create_generic.push({
        title: this.state.create_generic_title,
        subtitle: this.state.create_generic_subtitle,
        image_url: this.state.create_generic_image_url,
        buttons: [],
      })
      if (this.state.create_generic_button_type_1 == true) {
        this.state.create_generic[i].buttons.push({
          title: this.state.create_generic_button_title_1,
          type: 'web_url',
          webview_height_ratio: "tall",
          url: this.state.create_generic_button_url_1
        })
      }
      if (this.state.create_generic_button_type_1 == false) {
        this.state.create_generic[i].buttons.push({
          title: this.state.create_generic_button_title_1,
          type: 'postback',
          payload: this.state.create_generic_button_funtion_id_1.value
        })
      }
      if (this.state.create_generic_button_title_2 !== '') {
        if (this.state.create_generic_button_type_2 == true) {
          this.state.create_generic[i].buttons.push({
            title: this.state.create_generic_button_title_2,
            type: 'web_url',
            webview_height_ratio: "tall",
            url: this.state.create_generic_button_url_2
          })
        }
        if (this.state.create_generic_button_type_2 == false) {
          this.state.create_generic[i].buttons.push({
            title: this.state.create_generic_button_title_2,
            type: 'postback',
            payload: this.state.create_generic_button_funtion_id_2.value
          })
        }
      }
      if (this.state.create_generic_button_title_3 !== '') {
        if (this.state.create_generic_button_type_3 == true) {
          this.state.create_generic[i].buttons.push({
            title: this.state.create_generic_button_title_3,
            type: 'web_url',
            webview_height_ratio: "tall",
            url: this.state.create_generic_button_url_3
          })
        }
        if (this.state.create_generic_button_type_3 == false) {
          this.state.create_generic[i].buttons.push({
            title: this.state.create_generic_button_title_3,
            type: 'postback',
            payload: this.state.create_generic_button_funtion_id_3.value
          })
        }
      }
      this.setState({
        showActionAddGenericTemplate: true,
        showAddGenericTemplate: true,
        showActionAddGenericElement: false,
        showAddGenericElement: false,
        create_generic_title: '',
        create_generic_subtitle: '',
        create_generic_image_url: '',
        create_generic_button_title_1: '',
        create_generic_button_type_1: false,
        create_generic_button_url_1: '',
        create_generic_button_funtion_id_1: '',
        create_generic_button_title_2: '',
        create_generic_button_type_2: false,
        create_generic_button_url_2: '',
        create_generic_button_funtion_id_2: '',
        create_generic_button_title_3: '',
        create_generic_button_type_3: false,
        create_generic_button_url_3: '',
        create_generic_button_funtion_id_3: '',
      })
    } else {
      alert('Số thẻ tối đa là 10!');
    }
  }

  handleUpdateGenericElement = () => {
    var i = this.state.index,
      array = this.state.create_generic;
    array[i].title = this.state.create_generic_title;
    array[i].subtitle = this.state.create_generic_subtitle;
    array[i].image_url = this.state.create_generic_image_url;

    if (this.state.create_generic_button_type_1 == true) {
      array[i].buttons[0].title = this.state.create_generic_button_title_1;
      array[i].buttons[0].type = 'web_url';
      array[i].buttons[0].webview_height_ratio = "tall";
      array[i].buttons[0].url = this.state.create_generic_button_url_1;
    }
    if (this.state.create_generic_button_type_1 == false) {
      array[i].buttons[0].title = this.state.create_generic_button_title_1;
      array[i].buttons[0].type = 'postback';
      array[i].buttons[0].payload = this.state.create_generic_button_funtion_id_1.value;
    }
    if (this.state.create_generic_button_title_2 !== '') {
      if (this.state.create_generic_button_type_2 == true) {
        if (array[i].buttons[1]) {
          array[i].buttons[1].title = this.state.create_generic_button_title_2;
          array[i].buttons[1].type = 'web_url';
          array[i].buttons[1].webview_height_ratio = "tall";
          array[i].buttons[1].url = this.state.create_generic_button_url_2;
        } else {
          array[i].buttons.push({
            title: this.state.create_generic_button_title_2,
            type: 'web_url',
            webview_height_ratio: "tall",
            url: this.state.create_generic_button_url_2
          })
        }
      }
      if (this.state.create_generic_button_type_2 == false) {
        if (array[i].buttons[1]) {
          array[i].buttons[1].title = this.state.create_generic_button_title_2;
          array[i].buttons[1].type = 'postback';
          array[i].buttons[1].payload = this.state.create_generic_button_funtion_id_2.value;
        }
        else {
          array[i].buttons.push({
            title: this.state.create_generic_button_title_2,
            type: 'postback',
            payload: this.state.create_generic_button_funtion_id_2.value
          })
        }
      }
      if (this.state.create_generic_button_title_3 !== '') {
        if (this.state.create_generic_button_type_3 == true) {
          if (array[i].buttons[2]) {
            array[i].buttons[2].title = this.state.create_generic_button_title_3;
            array[i].buttons[2].type = 'web_url';
            array[i].buttons[2].webview_height_ratio = "tall";
            array[i].buttons[2].url = this.state.create_generic_button_url_3;
          } else {
            array[i].buttons.push({
              title: this.state.create_generic_button_title_3,
              type: 'web_url',
              webview_height_ratio: "tall",
              url: this.state.create_generic_button_url_3
            })
          }
        }
        if (this.state.create_generic_button_type_3 == false) {
          if (array[i].buttons[2]) {
            array[i].buttons[2].title = this.state.create_generic_button_title_3;
            array[i].buttons[2].type = 'postback';
            array[i].buttons[2].payload = this.state.create_generic_button_funtion_id_3.value;
          }
          else {
            array[i].buttons.push({
              title: this.state.create_generic_button_title_3,
              type: 'postback',
              payload: this.state.create_generic_button_funtion_id_3.value
            })
          }
        }
      }
    }
    if (this.state.create_generic_button_title_2 == '') {
      array[i].buttons.splice(1, 1);
      if (this.state.create_generic_button_title_3 !== '') {
        if (this.state.create_generic_button_type_3 == true) {
          if (array[i].buttons[1]) {
            array[i].buttons[1].title = this.state.create_generic_button_title_3;
            array[i].buttons[1].type = 'web_url';
            array[i].buttons[1].webview_height_ratio = "tall";
            array[i].buttons[1].url = this.state.create_generic_button_url_3;
          } else {
            array[i].buttons.push({
              title: this.state.create_generic_button_title_3,
              type: 'web_url',
              webview_height_ratio: "tall",
              url: this.state.create_generic_button_url_3
            })
          }
        }
        if (this.state.create_generic_button_type_3 == false) {
          if (array[i].buttons[1]) {
            array[i].buttons[1].title = this.state.create_generic_button_title_3;
            array[i].buttons[1].type = 'postback';
            array[i].buttons[1].payload = this.state.create_generic_button_funtion_id_3.value;
          }
          else {
            array[i].buttons.push({
              title: this.state.create_generic_button_title_3,
              type: 'postback',
              payload: this.state.create_generic_button_funtion_id_3.value
            })
          }
        }
      }
    }
    if (this.state.create_generic_button_title_3 == '') {
      if (array[i].buttons.length == 3) {
        array[i].buttons.splice(2, 1);
      } else {
        array[i].buttons.splice(1, 1);
      }
    }
    console.log(array);
    this.setState({
      showActionAddGenericTemplate: true,
      create_generic: array,
      showAddGenericTemplate: true,
      showActionAddTemplate: true,
      showAddGenericElement: false,
      showActionUpdateGenericElement: false,
      showActionUpdateTemplate: true,
      create_generic_title: '',
      create_generic_subtitle: '',
      create_generic_image_url: '',
      create_generic_button_title_1: '',
      create_generic_button_type_1: false,
      create_generic_button_url_1: '',
      create_generic_button_funtion_id_1: '',
      create_generic_button_title_2: '',
      create_generic_button_type_2: false,
      create_generic_button_url_2: '',
      create_generic_button_funtion_id_2: '',
      create_generic_button_title_3: '',
      create_generic_button_type_3: false,
      create_generic_button_url_3: '',
      create_generic_button_funtion_id_3: '',
    })
  }

  setTargetGroup = (data) => {
    var array = []
    for (let i = 0; i < data.length; i++) {
      array.push(data[i]._id);
    }
    this.setState({
      target_array: array
    })
    console.log(this.state.target_array)
  }

  onClickDeleteGenericElement = (event, rowData) => {
    var array = this.state.create_generic
    array.splice(rowData.tableData.id, 1);
    this.setState({ create_generic: array })
  }

  closeDialogUpdateText = () => {
    this.setState({
      dialogUpdateText: false,
      create_text: '',
    })
  }

  closeDialogUpdateMedia = () => {
    this.setState({
      dialogUpdateMedia: false,
      create_media_type: '',
      create_media_url: '',
    })
  }

  closeDialogUpdateList = () => {
    this.setState({
      dialogUpdateList: false,
      create_list_big_title: '',
      create_list_big_subtitle: '',
      create_list_big_image_url: '',
      create_list_big_url: '',
      create_list_content_1: '',
      create_list_subcontent_1: '',
      create_list_content_2: '',
      create_list_subcontent_2: '',
      create_list_content_3: '',
      create_list_subcontent_3: '',
      create_list_button_title: '',
      create_list_button_type: null,
      create_list_button_url: '',
      create_list_button_funtion_id: '',
    })
  }

  onCloseDialogShowUpdate = () => {
    this.setState({
      dialogShowUpdate: false,
      dialogLoading: false,
    });
  }

  addTextTemplate = () => {
    this.setState({
      showSelectionType: false,
      showAddTextTemplate: true,
      showActionAddText: true,
      showActionSelectType: false,
    })
  }

  closeAddText = () => {
    this.setState({
      showSelectionType: true,
      showAddTextTemplate: false,
      showActionAddText: false,
      showActionSelectType: true,
    })
  }

  addMediaTemplate = () => {
    this.setState({
      showSelectionType: false,
      showAddMediaTemplate: true,
      showActionAddMedia: true,
      showActionSelectType: false,
    })
  }

  closeAddMedia = () => {
    this.setState({
      showSelectionType: true,
      showAddMediaTemplate: false,
      showActionAddMedia: false,
      showActionSelectType: true,
    })
  }

  addListTemplate = () => {
    this.setState({
      showSelectionType: false,
      showActionSelectType: false,
      showActionAddListTemplate: true,
      showAddListTemplate: true,
    })
    this.getNode();
  }

  closeAddList = () => {
    this.setState({
      showSelectionType: true,
      showActionSelectType: true,
      showActionAddListTemplate: false,
      showAddListTemplate: false,
    })
  }

  addGenericTemplate = () => {
    this.setState({
      showSelectionType: false,
      showAddGenericTemplate: true,
      showActionAddGenericTemplate: true,
      showActionSelectType: false,
    })
    this.getNode();
  }

  closeAddGeneric = () => {
    this.setState({
      showSelectionType: true,
      showAddGenericTemplate: false,
      showActionAddGenericTemplate: false,
      showActionSelectType: true,
    })
  }

  createGenericElement = () => {
    this.setState({
      showActionAddTemplate: false,
      showActionUpdateTemplate: false,
      showAddGenericElement: true,
      showActionAddGenericElement: true,
      showAddGenericTemplate: false,
      showActionAddGenericTemplate: false,
    })
  }

  onClickUpdateGenericElement = (event, rowData) => {
    var i = rowData.tableData.id;
    this.setState({
      index: i,
      showActionAddTemplate: false,
      showAddGenericElement: true,
      showActionUpdateGenericElement: true,
      showActionUpdateTemplate: false,
      showAddGenericTemplate: false,
      showActionAddGenericTemplate: false,
      create_generic_title: this.state.create_generic[i].title,
      create_generic_subtitle: this.state.create_generic[i].subtitle,
      create_generic_image_url: this.state.create_generic[i].image_url,
      create_generic_button_title_1: this.state.create_generic[i].buttons[0].title,
    })
    if (this.state.create_generic[i].buttons[0].type == 'web_url') {
      this.setState({
        create_generic_button_type_1: true,
        create_generic_button_url_1: this.state.create_generic[i].buttons[0].url,
      })
    }
    if (this.state.create_generic[i].buttons[0].type == 'postback') {
      this.setState({
        create_generic_button_type_1: false,
        create_generic_button_funtion_id_1: {
          value: this.state.create_generic[i].buttons[0].payload,
          label: this.state.create_generic[i].buttons[0].payload,
        }
      })
    }
    if (this.state.create_generic[i].buttons[1]) {
      this.setState({ create_generic_button_title_2: this.state.create_generic[i].buttons[1].title })
      if (this.state.create_generic[i].buttons[1].type == 'web_url') {
        this.setState({
          create_generic_button_type_2: true,
          create_generic_button_url_2: this.state.create_generic[i].buttons[1].url,
        })
      }
      if (this.state.create_generic[i].buttons[1].type == 'postback') {
        this.setState({
          create_generic_button_type_2: false,
          create_generic_button_funtion_id_2: {
            value: this.state.create_generic[i].buttons[1].payload,
            label: this.state.create_generic[i].buttons[1].payload,
          }
        })
      }
    }
    if (this.state.create_generic[i].buttons[2]) {
      this.setState({ create_generic_button_title_3: this.state.create_generic[i].buttons[2].title })
      if (this.state.create_generic[i].buttons[2].type == 'web_url') {
        this.setState({
          create_generic_button_type_3: true,
          create_generic_button_url_3: this.state.create_generic[i].buttons[2].url,
        })
      }
      if (this.state.create_generic[i].buttons[2].type == 'postback') {
        this.setState({
          create_generic_button_type_3: false,
          create_generic_button_funtion_id_3: {
            value: this.state.create_generic[i].buttons[2].payload,
            label: this.state.create_generic[i].buttons[2].payload,
          }
        })
      }
    }
  }
  closeUpdateGenericElement = () => {
    this.setState({
      showAddGenericTemplate: true,
      showActionAddGenericTemplate: true,
      showAddGenericElement: false,
      showActionUpdateGenericElement: false,
      showActionUpdateTemplate: true,
      create_generic_title: '',
      create_generic_subtitle: '',
      create_generic_image_url: '',
      create_generic_button_title_1: '',
      create_generic_button_type_1: false,
      create_generic_button_url_1: '',
      create_generic_button_funtion_id_1: '',
      create_generic_button_title_2: '',
      create_generic_button_type_2: false,
      create_generic_button_url_2: '',
      create_generic_button_funtion_id_2: '',
      create_generic_button_title_3: '',
      create_generic_button_type_3: false,
      create_generic_button_url_3: '',
      create_generic_button_funtion_id_3: '',
      index: null
    })
  }

  closeAddGenericElement = () => {
    this.setState({
      showAddGenericTemplate: true,
      showActionAddGenericTemplate: true,
      showAddGenericElement: false,
      showActionAddGenericElement: false,
      showActionUpdateTemplate: true,
      create_generic_title: '',
      create_generic_subtitle: '',
      create_generic_image_url: '',
      create_generic_button_title_1: '',
      create_generic_button_type_1: false,
      create_generic_button_url_1: '',
      create_generic_button_funtion_id_1: '',
      create_generic_button_title_2: '',
      create_generic_button_type_2: false,
      create_generic_button_url_2: '',
      create_generic_button_funtion_id_2: '',
      create_generic_button_title_3: '',
      create_generic_button_type_3: false,
      create_generic_button_url_3: '',
      create_generic_button_funtion_id_3: '',
    })
  }

  openDialogCreateNoti = () => {
    this.setState({
      dialogCreateNoti: true,
    })
  }
  closeDialogCreateNoti = () => {
    this.setState({
      dialogCreateNoti: false,
      dialogShowUpdate: false,
      dialogAddTemplate: false,
      dialogUpdateText: false,
      dialogUpdateMedia: false,
      dialogUpdateList: false,
      dialogUpdateGeneric: false,
      showActionAddTemplate: true,
      showActionUpdateTemplate: true,
      showSelectionType: true,
      showAddTextTemplate: false,
      showAddMediaTemplate: false,
      showAddListTemplate: false,
      showAddGenericTemplate: false,
      showAddGenericElement: false,
      showActionAddGenericElement: false,
      showActionUpdateGenericElement: false,
      showActionAddMedia: false,
      showAddTarget: false,
      showActionTarget: false,
      index: null,
      index_leaf: null,
      create_text: '',
      create_media_type: '',
      create_media_url: '',
      create_list_big_title: '',
      create_list_big_subtitle: '',
      create_list_big_image_url: '',
      create_list_big_url: '',
      create_list_content_1: '',
      create_list_subcontent_1: '',
      create_list_content_2: '',
      create_list_subcontent_2: '',
      create_list_content_3: '',
      create_list_subcontent_3: '',
      create_list_button_title: '',
      create_list_button_type: true,
      create_list_button_url: '',
      create_list_button_funtion_id: '',
      create_generic: [],
      create_generic_title: '',
      create_generic_subtitle: '',
      create_generic_image_url: '',
      create_generic_button_title_1: '',
      create_generic_button_type_1: false,
      create_generic_button_url_1: '',
      create_generic_button_funtion_id_1: '',
      create_generic_button_title_2: '',
      create_generic_button_type_2: false,
      create_generic_button_url_2: '',
      create_generic_button_funtion_id_2: '',
      create_generic_button_title_3: '',
      create_generic_button_type_3: false,
      create_generic_button_url_3: '',
      create_generic_button_funtion_id_3: '',
      create_noti_name: '',
      create_hour: '*',
      create_minute: '*',
      create_second: '*',
      create_schedule_type: true,
      create_day_of_week: '*',
      create_day_of_month: '*',
      create_month: '*',
      create_status: true,
      showTempaltes: false,
      showActionSelectType: true,
      showActionAddText: false,
      create_value: null,
      create_target: false,
      showScheduleNoti: false,
      showActionScheduleNoti: false,
      tab_value: 0,
      node_array: [],
      leaf_data: [{
        TYPE: ''
      }],
      group_data: [],
      target_array: [],
      create_template: [],
    })
  }

  showAddTemplate = () => {
    this.setState({
      showSelectionType: true,
      showTempaltes: false,
      showActionSelectType: true,
      showActionAddTemplate: false,
    })
  }

  closeSelectType = () => {
    this.setState({
      showSelectionType: false,
      showActionSelectType: false,
      showActionAddTemplate: true,
      dialogCreateNoti: false,
      create_template: [],
    })
  }

  renderNoti() {
    const { classes } = this.props;
    return (
      <MaterialTable
        icons={tableIcons}
        title="Dữ liệu thông báo"
        columns={this.state.columns}
        data={this.state.noti_data}
        style={{}}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Thêm',
            isFreeAction: true,
            onClick: this.openDialogCreateNoti
          },
          rowData => ({
            icon: Edit,
            tooltip: 'Sửa',
            onClick: (event, rowData) => this.onClickUpdateNoti(event, rowData),
          }),
          rowData => ({
            icon: DeleteOutline,
            tooltip: 'Xoá',
            onClick: (event, rowData) => this.onClickDeleteNoti(event, rowData),
          })
        ]}
        options={{
          actionsColumnIndex: -1
        }}
      />
    )
  }

  renderContent() {
    const { classes } = this.props;
    return (
      <Card className={classes.content}>
        <Dialog
          open={this.state.dialogLoading}
        >
          <DialogContent>
            <HashLoader
              css={override}
              sizeUnit={"px"}
              size={70}
              color={'#da2128'}
              loading={true}
              className={classes.loader}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.showDialogUpdateNoti}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Sửa thông báo</DialogTitle>
          <DialogContent>
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <InputLabel htmlFor="outlined-age-simple" fullWidth>
                Trạng thái
              </InputLabel>
              <Select
                value={this.state.create_status}
                onChange={this.handleInputStatus}
                input={<OutlinedInput fullWidth name="Trạng thái" id="outlined-age-simple" />}
              >
                <MenuItem value={true}>Bật</MenuItem>
                <MenuItem value={false}>Tắt</MenuItem>
              </Select>
            </FormControl>
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="hour"
              label="Giờ 0-23"
              // type="password"
              id="hour"
              autoComplete="current-hour"
              value={this.state.create_hour}
              onChange={this.handleInputHour}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="minute"
              label="Phút 0-59"
              // type="password"
              id="minute"
              autoComplete="current-minute"
              value={this.state.create_minute}
              onChange={this.handleInputMinute}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="second"
              label="Giây 0-59"
              // type="password"
              id="second"
              autoComplete="current-second"
              value={this.state.create_second}
              onChange={this.handleInputSecond}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            <br />
            <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
              <InputLabel htmlFor="outlined-age-simple" fullWidth>
                Chọn loại đặt lịch
              </InputLabel>
              <Select
                value={this.state.create_schedule_type}
                onChange={this.handleInputScheduleType}
                input={<OutlinedInput fullWidth name="Loại đặt lịch" id="outlined-age-simple" />}
              >
                <MenuItem value={true}>Theo các ngày trong tuần</MenuItem>
                <MenuItem value={false}>Theo các ngày trong tháng</MenuItem>
              </Select>
            </FormControl>
            {
              this.state.create_schedule_type ?
                <div>
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="day_of_week"
                    label="Ngày trong tuần 2-8 (chỉ nhập 1 ngày). Nếu đặt hàng ngày thì nhập dấu sao '*'"
                    // type="password"
                    id="day_of_week"
                    autoComplete="current-day_of_week"
                    value={this.state.create_day_of_week}
                    onChange={this.handleInputDayOfWeek}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                </div> :
                <div>
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="day_of_month"
                    label="Ngày trong tháng 1-31 (nhập nhiều ngày cách nhau bằng dấu phẩy). Nếu đặt hàng ngày thì nhập dấu sao '*'"
                    // type="password"
                    id="day_of_month"
                    autoComplete="current-day_of_month"
                    value={this.state.create_day_of_month}
                    onChange={this.handleInputDayOfMonth}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="month"
                    label="Tháng 1-12. Nếu đặt hàng tháng thì nhập dấu sao '*'"
                    // type="password"
                    id="month"
                    autoComplete="current-month"
                    value={this.state.create_month}
                    onChange={this.handleInputMonth}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                </div>
            }
          </DialogContent>
          <DialogActions style={{ marginRight: '40%' }}>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.onClickCloseUpdateNoti} >
              Huỷ
            </Button>
            <Button variant="contained" className={classes.buttonFunction} onClick={this.updateNoti}>
              Cập nhật
            </Button>
            <Button variant="contained" className={classes.buttonFunction} style={{ minWidth: 200 }} onClick={this.pushNotiImmediately}>
              Đẩy thông báo ngay
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.dialogCreateNoti}
          //onClose={this.closeDialogCreateNoti}
          aria-labelledby="scroll-dialog-title"
          scroll="paper"
          fullWidth={true}
          maxWidth="xl"
        >
          <DialogTitle id="scroll-dialog-title">Tạo thông báo</DialogTitle>
          <DialogContent>
            <TextField
              //variant="outlined"
              margin="normal"
              required
              fullWidth
              name="noti_name"
              label="Tên thông báo"
              // type="password"
              id="noti_name"
              autoComplete="current-noti_name"
              value={this.state.create_noti_name}
              onChange={this.handleInputNotiName}
              style={{ marginLeft: '2%', width: '90%' }}
            />
            {
              this.state.showScheduleNoti ?
                <div>
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Trạng thái
                    </InputLabel>
                    <Select
                      value={this.state.create_status}
                      onChange={this.handleInputStatus}
                      input={<OutlinedInput fullWidth name="Trạng thái" id="outlined-age-simple" />}
                    >
                      <MenuItem value={true}>Bật</MenuItem>
                      <MenuItem value={false}>Tắt</MenuItem>
                    </Select>
                  </FormControl>
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="hour"
                    label="Giờ 0-23"
                    // type="password"
                    id="hour"
                    autoComplete="current-hour"
                    value={this.state.create_hour}
                    onChange={this.handleInputHour}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="minute"
                    label="Phút 0-59"
                    // type="password"
                    id="minute"
                    autoComplete="current-minute"
                    value={this.state.create_minute}
                    onChange={this.handleInputMinute}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="second"
                    label="Giây 0-59"
                    // type="password"
                    id="second"
                    autoComplete="current-second"
                    value={this.state.create_second}
                    onChange={this.handleInputSecond}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Chọn loại đặt lịch
                    </InputLabel>
                    <Select
                      value={this.state.create_schedule_type}
                      onChange={this.handleInputScheduleType}
                      input={<OutlinedInput fullWidth name="Loại đặt lịch" id="outlined-age-simple" />}
                    >
                      <MenuItem value={true}>Theo các ngày trong tuần</MenuItem>
                      <MenuItem value={false}>Theo các ngày trong tháng</MenuItem>
                    </Select>
                  </FormControl>
                  {
                    this.state.create_schedule_type ?
                      <div>
                        <br />
                        <TextField
                          //variant="outlined"
                          margin="normal"
                          required
                          fullWidth
                          name="day_of_week"
                          label="Ngày trong tuần 2-8 (chỉ nhập 1 ngày). Nếu đặt hàng ngày thì nhập dấu sao '*'"
                          // type="password"
                          id="day_of_week"
                          autoComplete="current-day_of_week"
                          value={this.state.create_day_of_week}
                          onChange={this.handleInputDayOfWeek}
                          style={{ marginLeft: '2%', width: '90%' }}
                        />
                      </div> :
                      <div>
                        <br />
                        <TextField
                          //variant="outlined"
                          margin="normal"
                          required
                          fullWidth
                          name="day_of_month"
                          label="Ngày trong tháng 1-31 (nhập nhiều ngày cách nhau bằng dấu phẩy). Nếu đặt hàng ngày thì nhập dấu sao '*'"
                          // type="password"
                          id="day_of_month"
                          autoComplete="current-day_of_month"
                          value={this.state.create_day_of_month}
                          onChange={this.handleInputDayOfMonth}
                          style={{ marginLeft: '2%', width: '90%' }}
                        />
                        <br />
                        <TextField
                          //variant="outlined"
                          margin="normal"
                          required
                          fullWidth
                          name="month"
                          label="Tháng 1-12. Nếu đặt hàng tháng thì nhập dấu sao '*'"
                          // type="password"
                          id="month"
                          autoComplete="current-month"
                          value={this.state.create_month}
                          onChange={this.handleInputMonth}
                          style={{ marginLeft: '2%', width: '90%' }}
                        />
                      </div>
                  }
                </div> : null
            }
            {
              this.state.showAddTarget ?
                <div>
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Chọn mục tiêu
                    </InputLabel>
                    <Select
                      value={this.state.create_target}
                      onChange={this.handleInputTarget}
                      input={<OutlinedInput fullWidth name="Mục tiêu" id="outlined-age-simple" />}
                    >
                      <MenuItem value={false}>Tất cả CBNV</MenuItem>
                      <MenuItem value={true}>Các nhóm</MenuItem>
                    </Select>
                  </FormControl>
                  {
                    this.state.create_target ?
                      <MaterialTable
                        icons={tableIcons}
                        title="Danh sách các nhóm"
                        columns={[
                          { title: 'Tên', field: 'NAME' },
                        ]}
                        data={this.state.group_data}
                        style={{}}
                        options={{
                          selection: true
                        }}
                        onSelectionChange={(rows) => this.setTargetGroup(rows)}
                      /> : null
                  }
                </div> : null
            }
            {
              this.state.showSelectionType ?
                <Grid container spacing={3}>
                  <Grid item xs={4}>
                    <Card className={classes.paper}>
                      <Typography variant="body2" color="textSecondary" component="p" style={{ marginTop: '2%', marginBottom: '2%' }}>
                        Loại GENERIC - Phức hợp
                      </Typography>
                      <CardMedia
                        className={classes.media_}
                        image={Generic_img}
                        title="Paella dish"
                      />
                      <Button variant="contained" className={classes.buttonFunction} onClick={this.addGenericTemplate}>
                        Chọn
                      </Button>
                    </Card>
                  </Grid>
                  <Grid item xs={4}>
                    <Card className={classes.paper}>
                      <Typography variant="body2" color="textSecondary" component="p" style={{ marginTop: '2%', marginBottom: '2%' }}>
                        Loại TEXT - Văn bản
                      </Typography>
                      <CardMedia
                        className={classes.media_}
                        image={Text_img}
                        title="Paella dish"
                      />
                      <Button variant="contained" className={classes.buttonFunction} onClick={this.addTextTemplate}>
                        Chọn
                      </Button>
                    </Card>
                  </Grid>
                  <Grid item xs={4}>
                    <Card className={classes.paper}>
                      <Typography variant="body2" color="textSecondary" component="p" style={{ marginTop: '2%', marginBottom: '2%' }}>
                        Loại MEDIA - Hình ảnh/Video
                      </Typography>
                      <CardMedia
                        className={classes.media_}
                        image={Media_img}
                        title="Paella dish"
                      />
                      <Button variant="contained" className={classes.buttonFunction} onClick={this.addMediaTemplate}>
                        Chọn
                      </Button>
                    </Card>
                  </Grid>
                  {/* <Grid item xs={3}>
                    <Card className={classes.paper}>
                      <Typography variant="body2" color="textSecondary" component="p" style={{ marginTop: '2%', marginBottom: '2%' }}>
                        Loại LIST - Danh sách
                      </Typography>
                      <CardMedia
                        className={classes.media_}
                        image={List_img}
                        title="Paella dish"
                      />
                      <Button variant="contained" className={classes.buttonFunction} onClick={this.addListTemplate}>
                        Chọn
                      </Button>
                    </Card>
                  </Grid> */}
                </Grid> : null
            }
            {
              this.state.showAddTextTemplate ?
                <TextField
                  //variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="text"
                  label="Tin nhắn văn bản"
                  multiline
                  rows="8"
                  rowsMax="10"
                  // type="password"
                  id="text"
                  autoComplete="current-text"
                  value={this.state.create_text}
                  onChange={this.handleInputText}
                  style={{ marginLeft: '2%', width: '90%' }}
                /> : null
            }
            {
              this.state.showAddMediaTemplate ?
                <div>
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Chọn loại
                  </InputLabel>
                    <Select
                      value={this.state.create_media_type}
                      onChange={this.handleInputMediaType}
                      input={<OutlinedInput fullWidth name="Loại" id="outlined-age-simple" />}
                    >
                      <MenuItem value={'image'}>Ảnh</MenuItem>
                      <MenuItem value={'video'}>Video</MenuItem>
                    </Select>
                  </FormControl>
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="media_url"
                    label="URL"
                    // type="password"
                    id="media_url"
                    autoComplete="current-media_url"
                    value={this.state.create_media_url}
                    onChange={this.handleInputMediaURL}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                </div> : null
            }
            {
              this.state.showAddListTemplate ?
                <div>
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="list_big_title"
                    label="Tiêu đề danh sách"
                    // type="password"
                    id="list_big_title"
                    autoComplete="current-list_big_title"
                    value={this.state.create_list_big_title}
                    onChange={this.handleInputListBigTitle}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="list_big_subtitle"
                    label="Phụ đề danh sách"
                    // type="password"
                    id="list_big_subtitle"
                    autoComplete="current-list_big_subtitle"
                    value={this.state.create_list_big_subtitle}
                    onChange={this.handleInputListBigSubtitle}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="list_big_image_url"
                    label="Ảnh bìa danh sách"
                    // type="password"
                    id="list_big_image_url"
                    autoComplete="current-list_big_image_url"
                    value={this.state.create_list_big_image_url}
                    onChange={this.handleInputListBigImageUrl}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="list_big_url"
                    label="Đường dẫn danh sách"
                    // type="password"
                    id="list_big_url"
                    autoComplete="current-list_big_url"
                    value={this.state.create_list_big_url}
                    onChange={this.handleInputListBigUrl}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="list_content_1"
                    label="Nội dung 1"
                    // type="password"
                    id="list_content_1"
                    autoComplete="current-list_content_1"
                    value={this.state.create_list_content_1}
                    onChange={this.handleInputListContent1}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="list_subcontent_1"
                    label="Nội dung phụ 1"
                    // type="password"
                    id="list_subcontent_1"
                    autoComplete="current-list_subcontent_1"
                    value={this.state.create_list_subcontent_1}
                    onChange={this.handleInputListSubContent1}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    // required
                    fullWidth
                    name="list_content_2"
                    label="Nội dung 2"
                    // type="password"
                    id="list_content_2"
                    autoComplete="current-list_content_2"
                    value={this.state.create_list_content_2}
                    onChange={this.handleInputListContent2}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    // required
                    fullWidth
                    name="list_subcontent_2"
                    label="Nội dung phụ 2"
                    // type="password"
                    id="list_subcontent_2"
                    autoComplete="current-list_subcontent_2"
                    value={this.state.create_list_subcontent_2}
                    onChange={this.handleInputListSubContent2}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    // required
                    fullWidth
                    name="list_content_3"
                    label="Nội dung 3"
                    // type="password"
                    id="list_content_3"
                    autoComplete="current-list_content_3"
                    value={this.state.create_list_content_3}
                    onChange={this.handleInputListContent3}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    // required
                    fullWidth
                    name="list_subcontent_3"
                    label="Nội dung phụ 3"
                    // type="password"
                    id="list_subcontent_3"
                    autoComplete="current-list_subcontent_3"
                    value={this.state.create_list_subcontent_3}
                    onChange={this.handleInputListSubContent3}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="list_button_title"
                    label="Tiêu đề nút"
                    // type="password"
                    id="list_button_title"
                    autoComplete="current-list_button_title"
                    value={this.state.create_list_button_title}
                    onChange={this.handleInputListButtonTitle}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Chọn loại nút
                    </InputLabel>
                    <Select
                      value={this.state.create_list_button_type}
                      onChange={this.handleInputListButtonType}
                      input={<OutlinedInput fullWidth id="outlined-age-simple" />}
                    >
                      <MenuItem value={true}>Mở web</MenuItem>
                    </Select>
                  </FormControl>
                  <br />
                  <br />
                  {
                    this.state.create_list_button_type ?
                      <TextField
                        //variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="list_button_url"
                        label="Đường dẫn của nút"
                        // type="password"
                        id="list_button_url"
                        autoComplete="current-list_button_url"
                        value={this.state.create_list_button_url}
                        onChange={this.handleInputListButtonUrl}
                        style={{ marginLeft: '2%', width: '90%' }}
                      /> :
                      <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                        <SelectSearch
                          placeholder="Mã chức năng"
                          style={{ marginLeft: '2%', width: '60%', color: '#ffffff' }}
                          value={this.state.create_list_button_funtion_id}
                          onChange={this.handleInputListButtonFunctionID}
                          options={this.state.node_array}
                        />
                      </FormControl>
                  }
                </div> : null
            }
            {
              this.state.showAddGenericTemplate ?
                <MaterialTable
                  icons={tableIcons}
                  title="Các thẻ của tin nhắn Phức hợp"
                  columns={[
                    { title: 'Tiêu đề', field: 'title' },
                    { title: 'Phụ đề', field: 'subtitle' },
                    { title: 'URL ảnh', field: 'image_url' },
                  ]}
                  data={this.state.create_generic}
                  style={{}}
                  actions={[
                    {
                      icon: AddBox,
                      tooltip: 'Thêm',
                      isFreeAction: true,
                      onClick: this.createGenericElement
                    },
                    rowData => ({
                      icon: Edit,
                      tooltip: 'Sửa',
                      onClick: (event, rowData) => this.onClickUpdateGenericElement(event, rowData),
                    }),
                    rowData => ({
                      icon: DeleteOutline,
                      tooltip: 'Xoá',
                      onClick: (event, rowData) => this.onClickDeleteGenericElement(event, rowData),
                    })
                  ]}
                  options={{
                    actionsColumnIndex: -1
                  }}
                /> : null
            }
            {
              this.state.showAddGenericElement ?
                <div>
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="generic_title"
                    label="Tiêu đề thẻ"
                    // type="password"
                    id="generic_title"
                    autoComplete="current-generic_title"
                    value={this.state.create_generic_title}
                    onChange={this.handleInputGenericTitle}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="generic_subtitle"
                    label="Phụ đề thẻ"
                    // type="password"
                    id="generic_subtitle"
                    autoComplete="current-generic_subtitle"
                    value={this.state.create_generic_subtitle}
                    onChange={this.handleInputGenericSubtitle}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="generic_image_ur"
                    label="URL ảnh của thẻ"
                    // type="password"
                    id="generic_image_ur"
                    autoComplete="current-generic_image_ur"
                    value={this.state.create_generic_image_url}
                    onChange={this.handleInputGenericImageUrl}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="generic_button_title_1"
                    label="Tiêu đề nút 1"
                    // type="password"
                    id="generic_button_title_1"
                    autoComplete="current-generic_button_title_1"
                    value={this.state.create_generic_button_title_1}
                    onChange={this.handleInputGenericButtonTitle1}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Chọn loại nút
                    </InputLabel>
                    <Select
                      value={this.state.create_generic_button_type_1}
                      onChange={this.handleInputGenericButtonType1}
                      input={<OutlinedInput fullWidth id="outlined-age-simple" />}
                    >
                      <MenuItem value={true}>Mở web</MenuItem>
                      <MenuItem value={false}>Mở chức năng</MenuItem>
                    </Select>
                  </FormControl>
                  <br />
                  <br />
                  {
                    this.state.create_generic_button_type_1 ?
                      <TextField
                        //variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="generic_button_url_1"
                        label="Đường dẫn của nút"
                        // type="password"
                        id="generic_button_url_1"
                        autoComplete="current-generic_button_url_1"
                        value={this.state.create_generic_button_url_1}
                        onChange={this.handleInputGenericButtonUrl1}
                        style={{ marginLeft: '2%', width: '90%' }}
                      /> :
                      <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                        <SelectSearch
                          placeholder="Mã chức năng"
                          style={{ marginLeft: '2%', width: '60%', }}
                          value={this.state.create_generic_button_funtion_id_1}
                          onChange={this.handleInputGenericButtonFunctionID1}
                          options={this.state.node_array}
                        />
                      </FormControl>
                  }
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    // required
                    fullWidth
                    name="generic_button_title_2"
                    label="Tiêu đề nút 2"
                    // type="password"
                    id="generic_button_title_2"
                    autoComplete="current-generic_button_title_2"
                    value={this.state.create_generic_button_title_2}
                    onChange={this.handleInputGenericButtonTitle2}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Chọn loại nút
                    </InputLabel>
                    <Select
                      value={this.state.create_generic_button_type_2}
                      onChange={this.handleInputGenericButtonType2}
                      input={<OutlinedInput fullWidth id="outlined-age-simple" />}
                    >
                      <MenuItem value={true}>Mở web</MenuItem>
                      <MenuItem value={false}>Mở chức năng</MenuItem>
                    </Select>
                  </FormControl>
                  <br />
                  <br />
                  {
                    this.state.create_generic_button_type_2 ?
                      <TextField
                        //variant="outlined"
                        margin="normal"
                        // required
                        fullWidth
                        name="generic_button_url_2"
                        label="Đường dẫn của nút"
                        // type="password"
                        id="generic_button_url_2"
                        autoComplete="current-generic_button_url_2"
                        value={this.state.create_generic_button_url_2}
                        onChange={this.handleInputGenericButtonUrl2}
                        style={{ marginLeft: '2%', width: '90%' }}
                      /> :
                      <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                        <SelectSearch
                          placeholder="Mã chức năng"
                          style={{ marginLeft: '2%', width: '60%', }}
                          value={this.state.create_generic_button_funtion_id_2}
                          onChange={this.handleInputGenericButtonFunctionID2}
                          options={this.state.node_array}
                        />
                      </FormControl>
                  }
                  <br />
                  <TextField
                    //variant="outlined"
                    margin="normal"
                    // required
                    fullWidth
                    name="generic_button_title_3"
                    label="Tiêu đề nút 3"
                    // type="password"
                    id="generic_button_title_3"
                    autoComplete="current-generic_button_title_3"
                    value={this.state.create_generic_button_title_3}
                    onChange={this.handleInputGenericButtonTitle3}
                    style={{ marginLeft: '2%', width: '90%' }}
                  />
                  <br />
                  <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                    <InputLabel htmlFor="outlined-age-simple" fullWidth>
                      Chọn loại nút
                    </InputLabel>
                    <Select
                      value={this.state.create_generic_button_type_3}
                      onChange={this.handleInputGenericButtonType3}
                      input={<OutlinedInput fullWidth id="outlined-age-simple" />}
                    >
                      <MenuItem value={true}>Mở web</MenuItem>
                      <MenuItem value={false}>Mở chức năng</MenuItem>
                    </Select>
                  </FormControl>
                  <br />
                  <br />
                  {
                    this.state.create_generic_button_type_3 ?
                      <TextField
                        //variant="outlined"
                        margin="normal"
                        // required
                        fullWidth
                        name="generic_button_url_3"
                        label="Đường dẫn của nút"
                        // type="password"
                        id="generic_button_url_3"
                        autoComplete="current-generic_button_url_3"
                        value={this.state.create_generic_button_url_3}
                        onChange={this.handleInputGenericButtonUrl3}
                        style={{ marginLeft: '2%', width: '90%' }}
                      /> :
                      <FormControl fullWidth className={classes.formControl} style={{ marginLeft: '2%', width: '90%' }}>
                        <SelectSearch
                          placeholder="Mã chức năng"
                          style={{ marginLeft: '2%', width: '60%', }}
                          value={this.state.create_generic_button_funtion_id_3}
                          onChange={this.handleInputGenericButtonFunctionID3}
                          options={this.state.node_array}
                        />
                      </FormControl>
                  }
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                </div> : null
            }
            {
              this.state.showActionScheduleNoti ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeScheduleNoti} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.pushScheduleNoti}>
                    Thêm
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionUpdateGenericElement ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeUpdateGenericElement} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.handleUpdateGenericElement}>
                    Thêm
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionAddText ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeAddText} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.handleAddTemplate}>
                    Thêm
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionAddMedia ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeAddMedia} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.handleAddTemplate}>
                    Thêm
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionAddListTemplate ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeAddList} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.handleAddTemplate}>
                    Thêm
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionAddGenericTemplate ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeAddGeneric} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.handleAddTemplate}>
                    Thêm
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionSelectType ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeDialogCreateNoti} >
                    Huỷ
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionTarget ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeTarget} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.pushNotiImmediately} >
                    Đẩy thông báo ngay
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.scheduleNoti} >
                    Đặt lịch đẩy thông báo
                </Button>
                </DialogActions> : null
            }
            {
              this.state.showActionAddGenericElement ?
                <DialogActions style={{ marginRight: '40%' }}>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.closeAddGenericElement} >
                    Huỷ
                </Button>
                  <Button variant="contained" className={classes.buttonFunction} onClick={this.handleAddGenericElement}>
                    Thêm
                </Button>
                </DialogActions> : null
            }
          </DialogContent>
        </Dialog>
        <CardContent>
          {
            // this.renderTemplate(this.state.leaf_data)
            this.renderNoti()
          }
        </CardContent>
      </Card>
    )
  }

  render() {
    if (this.state.permission.indexOf('NOTI') == -1) {
      return <h2>&emsp;&emsp;&emsp;Bạn không có quyền truy cập chức năng này!</h2>;
    } else {
      const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    Quản lý Thông báo
                </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Tabs value={this.state.tab_value} textColor="inherit">
              <Tab textColor="inherit" label="Thông báo" />
            </Tabs>
          </AppBar>
          {this.state.tab_value === 0 && this.renderContent()}
        </React.Fragment>
      )
    }
  }
}
Notification.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Notification);