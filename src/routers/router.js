import React from 'react';

import MyAccount from '../components/my_account/MyAccount';
import ChangePassword from '../components/my_account/ChangePassword';
import ShowAllAdmin from '../components/admin/ShowAllAdmin';
import HandBook from '../components/hand_book/HandBook';
import AITrainning from '../components/ai_trainning/AITrainning';
import User from '../components/user/User';
import Group from '../components/group/Group';
import Leaf from '../components/leaf/Leaf';
import Notification from '../components/notification/Notification';
import Statistic from '../components/statistic/Statistic';
import Scheduler from '../components/scheduler/Scheduler';

const routes = [
  {
    path: "/",
    exact: true,
    sidebar: () => <div>Tài khoản</div>,
    main: () => <MyAccount />
  },
  {
    path: "/my-account",
    exact: true,
    sidebar: () => <div>Tài khoản</div>,
    main: () => <MyAccount />
  },
  {
    path: "/change-password",
    sidebar: () => <div>Đổi mật khẩu</div>,
    main: () => <ChangePassword />
  },
  {
    path: "/admin-managament",
    sidebar: () => <div>Quản trị viên</div>,
    main: () => <ShowAllAdmin />
  },
  {
    path: "/user-management",
    sidebar: () => <div>Người dùng</div>,
    main: () => <User />
  },
  {
    path: "/group-management",
    sidebar: () => <div>Nhóm</div>,
    main: () => <Group />
  },
  {
    path: "/data-tree-management",
    sidebar: () => <div>Dữ liệu chatbot</div>,
    main: () => <Leaf />
  },
  // {
  //   path: "/data-AI-management",
  //   sidebar: () => <div>Huấn luyện chatbot</div>,
  //   main: () => <AITrainning />
  // },
  // {
  //   path: "/schedule-trainning-AI-management",
  //   sidebar: () => <div>Lập lịch huấn luyện</div>,
  //   main: () => <Scheduler />
  // },
  {
    path: "/handbook-management",
    sidebar: () => <div>Cẩm nang tư vấn</div>,
    main: () => <HandBook />
  },
  {
    path: "/notification-management",
    sidebar: () => <div>Thông báo</div>,
    main: () => <Notification />
  },
  {
    path: "/statistic-management",
    sidebar: () => <div>Thống kê</div>,
    main: () => <Statistic />
  },
];

export default routes;