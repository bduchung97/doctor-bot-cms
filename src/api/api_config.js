// const SERVER_URL = 'https://msb-la-chatbot-ai-2019.appspot.com',
//   SERVER_PORT = '';
// const SERVER_URL = 'http://localhost',
//   SERVER_PORT = ':1337';
const SERVER_URL = 'https://nino-chatbot-server.herokuapp.com',
  SERVER_PORT = '';

module.exports = {
  login_api: SERVER_URL + SERVER_PORT + '/post_admin_login',
  logout_api: SERVER_URL + SERVER_PORT + '/post_admin_logout',

  change_password_admin: SERVER_URL + SERVER_PORT + '/put_admin_password_update',
  show_all_admin: SERVER_URL + SERVER_PORT + '/post_admin_show_all',
  create_admin: SERVER_URL + SERVER_PORT + '/post_admin_create',
  delete_admin: SERVER_URL + SERVER_PORT + '/post_admin_delete',
  update_admin: SERVER_URL + SERVER_PORT + '/put_admin_update',

  show_all_handbook: SERVER_URL + SERVER_PORT + '/post_handbook_show_all',
  create_handbook: SERVER_URL + SERVER_PORT + '/post_handbook_create',
  update_handbook: SERVER_URL + SERVER_PORT + '/put_handbook_update',
  delete_handbook: SERVER_URL + SERVER_PORT + '/post_handbook_delete',
  upload_handbook: SERVER_URL + SERVER_PORT + '/post_handbook_upload',

  show_all_ai_trainning: SERVER_URL + SERVER_PORT + '/post_ai_trainning_show_all',
  upload_ai_trainning: SERVER_URL + SERVER_PORT + '/post_ai_trainning_upload',
  create_ai_trainning: SERVER_URL + SERVER_PORT + '/post_ai_trainning_create',
  update_ai_trainning: SERVER_URL + SERVER_PORT + '/put_ai_trainning_update',
  delete_ai_trainning: SERVER_URL + SERVER_PORT + '/post_ai_trainning_delete',
  show_approve_ai_trainning: SERVER_URL + SERVER_PORT + '/post_ai_training_not_approve_show_all',
  approve_ai_trainning: SERVER_URL + SERVER_PORT + '/put_ai_training_approve',

  show_all_user: SERVER_URL + SERVER_PORT + '/post_user_show_all',
  update_user: SERVER_URL + SERVER_PORT + '/put_user_update',

  show_all_group: SERVER_URL + SERVER_PORT + '/post_group_show_all',
  show_user_create_group: SERVER_URL + SERVER_PORT + '/post_user_show_all',
  create_group: SERVER_URL + SERVER_PORT + '/post_group_create',
  delete_group: SERVER_URL + SERVER_PORT + '/post_group_delete',
  update_group: SERVER_URL + SERVER_PORT + '/put_group_update',
  remove_member_group: SERVER_URL + SERVER_PORT + '/post_group_delete_multiple_members',
  add_member_group: SERVER_URL + SERVER_PORT + '/post_group_add_multiple_members',

  show_leaf: SERVER_URL + SERVER_PORT + '/post_leaf_show',
  update_template: SERVER_URL + SERVER_PORT + '/put_template_update',
  update_persistent_menu: SERVER_URL + SERVER_PORT + '/put_persistent_menu_update',
  show_node_all: SERVER_URL + SERVER_PORT + '/post_node_show_all',
  create_template: SERVER_URL + SERVER_PORT + '/post_template_create',
  delete_template: SERVER_URL + SERVER_PORT + '/post_template_delete',
  update_default_leaf: SERVER_URL + SERVER_PORT + '/put_default_leaf_update',
  show_leaf_all: SERVER_URL + SERVER_PORT + '/post_leaf_show_all',
  show_node_all_: SERVER_URL + SERVER_PORT + '/post_node_show_all_',
  create_leaf: SERVER_URL + SERVER_PORT + '/post_leaf_create',
  delete_leaf: SERVER_URL + SERVER_PORT + '/post_leaf_delete',
  update_node: SERVER_URL + SERVER_PORT + '/put_node_update',
  upload_node: SERVER_URL + SERVER_PORT + '/post_node_upload',

  show_noti_all: SERVER_URL + SERVER_PORT + '/post_notification_show_all',
  show_group_noti: SERVER_URL + SERVER_PORT + '/post_group_show_all_',
  push_noti_immediate: SERVER_URL + SERVER_PORT + '/post_notification_create_immediate',
  push_noti: SERVER_URL + SERVER_PORT + '/post_notification_create',
  delete_noti: SERVER_URL + SERVER_PORT + '/post_notification_delete',
  update_noti: SERVER_URL + SERVER_PORT + '/post_notification_update',
  push_noti_now: SERVER_URL + SERVER_PORT + '/post_notification_push_immediate',

  show_statistic_all: SERVER_URL + SERVER_PORT + '/post_statistics_show_all',

  show_scheduler: SERVER_URL + SERVER_PORT + '/post_train_AI_scheduler_show',
  update_scheduler: SERVER_URL + SERVER_PORT + '/put_train_AI_scheduler_update',

}

